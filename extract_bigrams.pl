#!/usr/bin/env perl

use strict;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use open ':utf8';

my $stoplistfile=shift;

print STDERR "BIGRAMS EXTRACTOR --- stoplistfile=$stoplistfile\n";

my $forme; my $lemme; my $tag; my $proba; my $cat;
my $tok="";

print STDERR "\tLoading stoplist file...";
my %stoplist_proba_tag_sachant_forme_FT=();
open(STOPLIST,"< $stoplistfile") || die("'$stoplistfile': $!\n");
while (<STOPLIST>) {
    chomp;
    if (/^([^\t]*)\t([^\t]*)$/) {
	$stoplist_proba_tag_sachant_forme_FT{$1}{$2}++;
	$stoplist_proba_tag_sachant_forme_FT{$1}{__ALL__}++;
    }
}
close(STOPLIST);
for $forme (keys %stoplist_proba_tag_sachant_forme_FT) {
    for $tag (keys %{$stoplist_proba_tag_sachant_forme_FT{$forme}}) {
	if ($tag ne "__ALL__") {$stoplist_proba_tag_sachant_forme_FT{$forme}{$tag}/=$stoplist_proba_tag_sachant_forme_FT{$forme}{__ALL__}}
    }
}
for $forme (keys %stoplist_proba_tag_sachant_forme_FT) {
    delete($stoplist_proba_tag_sachant_forme_FT{$forme}{__ALL__});
}

print STDERR "done\n\tLoading input file and pushing bigrams...\r";
my %proba_tag_sachant_forme_T=();
my %proba_prevtag_sachant_prevforme_T=();
my $prevtag="";
my $prevtok="";
my %bigramocclr=();
my %bigramoccrl=();
my %bigramprobalr=();
my %bigramprobarl=();
my %occ_forme=();
my $n=0;
my $p;
my @tag=();
my @temp;
my $subtagnum;
my $prevtokhastags=0;
my $tokhastags=0;
my $l=0;
while(<STDIN>) {
    if ($l++ % 1000 == 0) {print STDERR "\tLoading input file and pushing bigrams...".($l-1)."\r"}
    chomp;
    if (/./) {
	$prevtok=$tok;
	$prevtokhastags=$tokhastags;
	%proba_prevtag_sachant_prevforme_T=%proba_tag_sachant_forme_T;
	/^([^\t]*)\t(.*)$/;
	$tok=$1;
	%proba_tag_sachant_forme_T=();
	@temp=split(/\|/,$2);
	for my $i (0..$#temp) {
	    $temp[$i]=~/^([^\t]*)\t([^\t]*)$/ || die ("Bad format in input file: ltp $_\n");
	    $tag=$1;
	    $proba=$2;
	    $proba_tag_sachant_forme_T{$tag}+=$proba;
	}

	$occ_forme{"{".$tok."}"}++;
	$occ_forme{__ALL__}++;

	$tokhastags=(!defined($proba_tag_sachant_forme_T{"?"}));
	if ($prevtok ne "") {
	    if ($prevtokhastags) { # si le tok pr�c�dent a des tags
		for $prevtag (keys %proba_prevtag_sachant_prevforme_T) {
		    if ($tokhastags) { # si le tok courant a des tags
			for $tag (keys %proba_tag_sachant_forme_T) {
			    $p=$proba_prevtag_sachant_prevforme_T{$prevtag}*$proba_tag_sachant_forme_T{$tag};
			    &loadbigram(\%bigramoccrl,$tag,$prevtag,$p);
			    &loadbigram(\%bigramocclr,$prevtag,$tag,$p);
			}
#		    } else { # si le tok courant n'a pas de tags
#			$p=$proba_prevtag_sachant_prevforme_T{$prevtag};
#			&loadbigram(\%bigramoccrl,"?",$prevtag,$p);
#			&loadbigram(\%bigramocclr,$prevtag,"?",$p);
		    }
		}
#	    } else { # si le tok pr�c�dent n'a pas de tag
#		if ($tokhastags) { # si le tok courant a des tags
#		    for $tag (keys %proba_tag_sachant_forme_T) {
#			$p=$proba_tag_sachant_forme_T{$tag};
#			&loadbigram(\%bigramoccrl,$tag,"?",$p);
#			&loadbigram(\%bigramocclr,"?",$tag,$p);
#		    }
#		} else { # si le tok courant n'a pas de tags
#		    $p=1;
#		    &loadbigram(\%bigramoccrl,"?","?",$p);
#		    &loadbigram(\%bigramocclr,"?","?",$p);
#		}
	    }
	}
    }
}


print STDERR "\tLoading input file and pushing bigrams...done          \n\tNormalization and filtering of rare bigrams or unnecessary word-like-tags...";
# normalisation et filtrage des bigrammes trop rares
my $all="";
&normalize_and_filter(\%bigramocclr,\%bigramprobalr);
&normalize_and_filter(\%bigramoccrl,\%bigramprobarl);

# seconde normalisation et output
print STDERR "done\n\tRe-normalization and output...";
&renormalize(\%bigramprobalr,">");
&renormalize(\%bigramprobarl,"<");
print STDERR "done\n";

sub loadbigram {
    my $bigramocchref=shift;
    my $pt=shift; # prev token
    my $t=shift;  # (cur)token
    my $localp=shift; # proba locale
    my @tag=();
    my $subtagnum;
    #print STDERR "loadbigram($bigramocchref,$pt,$t,$localp)\n";
    if ($localp>0) {
	$bigramocchref->{$pt}{$t}+=$p;
	$bigramocchref->{$pt}{__ALL__}+=$p;
	@tag=split(/-/,$t);
	for $subtagnum (0..$#tag) {
	    $bigramocchref->{$pt}{$subtagnum.":".$tag[$subtagnum]}+=$localp;
	}
    }
}

sub deserves_its_own_tag {
    my $t=shift; # token
    return ($occ_forme{$t}>$occ_forme{__ALL__}/2000);
}

sub normalize_and_filter {
    my $bigramocchref=shift;
    my $bigramprobahref=shift;
    for my $knowntok (keys %$bigramocchref) {
	for my $tok (keys %{$bigramocchref->{$knowntok}}) {
	    if ($tok!~/__ALL__$/ && $bigramocchref->{$knowntok}{__ALL__}>0) {
		if ($knowntok!~/^(?:[0-9]+:)?\{.*\}$/ || &deserves_its_own_tag($knowntok)) {
		    if ($tok!~/^(?:[0-9]+:)?\{.*\}$/ || &deserves_its_own_tag($tok)) {
			$bigramprobahref->{$knowntok}{$tok}=$bigramocchref->{$knowntok}{$tok}/$bigramocchref->{$knowntok}{__ALL__};
			if ($bigramprobahref->{$knowntok}{$tok}>0) {
			    $all="__ALL__";
			    if ($tok=~/^([0-9]+:)/) {$all=$1.$all;}
			    $bigramprobahref->{$knowntok}{$all}+=$bigramprobahref->{$knowntok}{$tok};
			} else {
			    delete($bigramprobahref->{$knowntok}{$tok}); # proba du bigramme = 0
			}
		    } else {
			delete($bigramprobahref->{$knowntok}{$tok}); # $tok ne m�rite pas un tag de forme {$tok}
		    }
		} else {
		    delete($bigramprobahref->{$knowntok}{$tok}); # $knowntok ne m�rite pas un tag de forme {$knowntok}
		}
	    }
	}
    }
}

sub renormalize {
    my $bigramprobahref=shift;
    my $direction=shift;
    for my $knowntok (keys %$bigramprobahref) {
	for my $tok (keys %{$bigramprobahref->{$knowntok}}) {
	    if ($tok!~/__ALL__$/ && $bigramprobahref->{$knowntok}{__ALL__}>0) {
		$all="__ALL__";
		if ($tok=~/^([0-9]+:)/) {$all=$1.$all;}
		$bigramprobahref->{$knowntok}{$tok}/=$bigramprobahref->{$knowntok}{$all};
		print "$direction\t$knowntok\t$tok\t".($bigramprobahref->{$knowntok}{$tok})."\n";
	    }
	}
    }
}
