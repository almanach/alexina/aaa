#!/usr/bin/env perl -w

use CGI qw/:standard Vars/;
use strict;

print header;
print start_html("Validation result");

print "Saving sent information<br>\n";
#print $ENV{QUERY_STRING}."\n";


my %FORM = Vars();
if ($FORM{isfirstpage}==1) {
    my $time="";
    if ($time=`ls -ldi /var/tmp/validationresult 2>/dev/null`) {
	chomp $time;
	if ($time ne "") {
	    $time=~s/^([^ ]+) [^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +(.*) \/var\/tmp\/validationresult.*$/$1\__$2/;
	    $time=~s/ /_/g;
	    $time=~s/:/-/g;
	    system("mv /var/tmp/validationresult /var/tmp/validationresult.$time");
	}
    }
}

open(RESULT,">>/var/tmp/validationresult") || die("Could not open validationresult\n");

for ($FORM{firstlemma}..($FORM{firstlemma}+$FORM{nlemmas}-1)) {
    print RESULT $FORM{$_}.$FORM{"line$_"}."\n";
}
close(RESULT);
print "<meta http-equiv=\"refresh\" content=\"0;URL=../$FORM{nextpage}\">\n";
print end_html;
