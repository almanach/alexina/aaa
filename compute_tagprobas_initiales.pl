#!/usr/bin/env perl

use strict;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use open ':utf8';

my $tagdatafile=shift;
my $stoplistfile=shift;
my $occsfile=shift;

print STDERR "COMPUTE INITIAL TAG PROBAS --- tagdatafile=$tagdatafile stoplistfile=$stoplistfile occsfile=$occsfile\n";

my $forme;
my $lemme;
my $proba;
my $tag;
my %proba_tag_sachant_forme_FICHIER_FLt=();
my @proba_tag_sachant_forme_FnLt=();
my %lemmes_et_tags_sachant_forme=();
my %stoplist_proba_tag_sachant_forme_FT=();
my %occ_forme=();
my @token=();

# lecture du nombre d'occurrences de chaque forme
open(OCC, $occsfile) || die("$occsfile non trouv� $!\n"); #ouverture du fichier en lecture  
my %occurrences=();
while (<OCC>) { 
    chomp($_); 
    $_ =~ /^([^\t]*)\t([^\t]*)$/ || die("Bad format in occurrences file: $_\n");
    $occ_forme{$1}=$2;
    $occ_forme{__ALL__}+=$2;
}
close(OCC);

# lecture de la stoplist
print STDERR "\tLoading stoplist (closed-class words)...";
my %stoplist_proba_tag_sachant_forme_FT=();
open(STOPLIST,"< $stoplistfile") || die("'$stoplistfile': $!\n");
while (<STOPLIST>) {
    chomp;
    if (/^([^\t]*)\t([^\t]*)$/) {
	$stoplist_proba_tag_sachant_forme_FT{$1}{$2}++;
	$stoplist_proba_tag_sachant_forme_FT{$1}{__ALL__}++;
    }
}
close(STOPLIST);
my $forme; my $tag;
for $forme (keys %stoplist_proba_tag_sachant_forme_FT) {
    for $tag (keys %{$stoplist_proba_tag_sachant_forme_FT{$forme}}) {
	if ($tag ne "__ALL__") {$stoplist_proba_tag_sachant_forme_FT{$forme}{$tag}/=$stoplist_proba_tag_sachant_forme_FT{$forme}{__ALL__}}
    }
}
for $forme (keys %stoplist_proba_tag_sachant_forme_FT) {
    delete($stoplist_proba_tag_sachant_forme_FT{$forme}{__ALL__});
}

print STDERR "done\n\tLoading tagdata file...";
open(TAGDATA,"< $tagdatafile");
# while (<TAGDATA>) {
#     chomp;
#     /^([^\t]+)\t([^\t]*\t[^\t]+)\t([^\t]+)\t([^\t]+)$/ || die("Bad format in file $tagdatafile: line $_");
#     $forme=$1;
#     $lemme=$2;
#     $tag=$3;
#     $proba=$4;
#     if (!(&deserves_its_own_tag($forme) && defined($stoplist_proba_tag_sachant_forme_FT{$forme}))) {
# 	if ($lemme=~/tag_unknown/) {
# 	    $lemme_et_tag="?\t?\t?";
# 	    $proba_lemme_et_tag_sachant_forme_FICHIER_FLt{$forme}{$lemme_et_tag}=$proba;
# 	} else {
# 	    $lemme_et_tag=$lemme."\t".$tag;
# 	    $proba_lemme_et_tag_sachant_forme_FICHIER_FLt{$forme}{$lemme_et_tag}=$proba;
# 	}
#     }
# }
while (<TAGDATA>) {
    chomp;
    /^([^\t]+)\t([^\t]+)\t([^\t]+)$/ || die("Bad format in file $tagdatafile: line $_");
    $forme=$1;
    $tag=$2;
    $proba=$3;
    if (!(&deserves_its_own_tag($forme) && defined($stoplist_proba_tag_sachant_forme_FT{$forme}))) {
	if ($lemme=~/tag_unknown/) {
	    $proba_tag_sachant_forme_FICHIER_FLt{$forme}{$tag}=$proba;
	} else {
	    $proba_tag_sachant_forme_FICHIER_FLt{$forme}{$tag}=$proba;
	}
    }
}
close(TAGDATA);

print STDERR "done\n\tLoading corpus...\r";
my $tn=0;
my $bool=0;
while (<>) {
    chomp;
    if (/[^ \t]/) {
	$token[$tn]=$_;
	$bool=0;
	for $tag (keys %{$proba_tag_sachant_forme_FICHIER_FLt{$_}}) {
	    $bool=1;
	    $proba_tag_sachant_forme_FnLt[$tn]{$tag}=$proba_tag_sachant_forme_FICHIER_FLt{$_}{$tag};
	    $proba_tag_sachant_forme_FnLt[$tn]{__ALL__}+=$proba_tag_sachant_forme_FICHIER_FLt{$_}{$tag};
	}
	for $tag (keys %{$stoplist_proba_tag_sachant_forme_FT{$_}}) {
	    $bool=1;
	    if (&deserves_its_own_tag($_)) {
		$proba_tag_sachant_forme_FnLt[$tn]{"\{$_\}"}=1;
		$proba_tag_sachant_forme_FnLt[$tn]{__ALL__}=1;
	    } else {
		$proba_tag_sachant_forme_FnLt[$tn]{$tag}=$stoplist_proba_tag_sachant_forme_FT{$_}{$tag};
		$proba_tag_sachant_forme_FnLt[$tn]{__ALL__}+=$stoplist_proba_tag_sachant_forme_FT{$_}{$tag};
	    }
	}
	if (!$bool) {
	    if (&deserves_its_own_tag($_)) {
		$proba_tag_sachant_forme_FnLt[$tn]{"\{$_\}"}=1;
	    } else {
		$proba_tag_sachant_forme_FnLt[$tn]{"?"}=1;
	    }
	    $proba_tag_sachant_forme_FnLt[$tn]{__ALL__}=1;
	}
	$tn++;
	if ($tn % 1000 == 0) {print STDERR "\tLoading corpus...$tn\r";}
    }
}

print STDERR "\tLoading corpus...done    \n\tNormalizing probas...";
for $tn (0..$#proba_tag_sachant_forme_FnLt) {
    for (keys %{$proba_tag_sachant_forme_FnLt[$tn]}) {
	if ($_ ne "__ALL__") {
	    $proba_tag_sachant_forme_FnLt[$tn]{$_}/=$proba_tag_sachant_forme_FnLt[$tn]{__ALL__};
	}
    }
}
for $tn (0..$#proba_tag_sachant_forme_FnLt) {
    delete($proba_tag_sachant_forme_FnLt[$tn]{__ALL__});
}
print STDERR "done\n\tOutput...";
for $tn (0..$#proba_tag_sachant_forme_FnLt) {
    print $token[$tn]."\t";
    $bool=0;
    for (keys %{$proba_tag_sachant_forme_FnLt[$tn]}) {
	if ($bool) {print "|";}
	print $_."\t".$proba_tag_sachant_forme_FnLt[$tn]{$_};
	$bool=1;
    }
    print "\n";
}
print STDERR "done\n";


sub deserves_its_own_tag {
    my $t=shift; # token
#    if ($occ_forme{$t}>$occ_forme{__ALL__}/1000) {print STDERR "$t\t$occ_forme{$t}  >?  ".($occ_forme{__ALL__}/1000)."\n";}
    return ($occ_forme{$t}>$occ_forme{__ALL__}/2000);
}

