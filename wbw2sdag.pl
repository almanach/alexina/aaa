$tagdata2file=shift;
$occfile=shift;

open(TAGS,"<$tagdata2file") || die("error\n");
open(OCC,"<$occfile") || die("error\n");

while (<TAGS>) {
    chomp;
    /^([^\t]+)\t(.*)$/;
    $tags{$1}{$2}=1;
}

while (<OCC>) {
    chomp;
    /^([^\t]+)\t(.*)$/;
    $occ{$1}=$2;
}

$n=0;
while (<>) {
    chomp;
    next if /^$/;
#    if (/^[\.\;\!\?]+$/) {
#	print "$n\n";
#	$n=0;
#    }
    if (defined($tags{$_})) {
	for $t (keys %{$tags{$_}}) {
	    print $n."\t".($n+1)."\t$_\t$t\n";
	}
    } elsif ($occ{$_}>100) {
	print $n."\t".($n+1)."\t$_\t{$_}\t0\n";
    } else {
	print $n."\t".($n+1)."\t$_\tuw\t0\n";
    }
    $n++;
}
