use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

print STDERR "  Parsing data file...";
while (<STDIN>){
    if ($l % 10000 == 0) {print STDERR "\r  Parsing data file...$l";}
    $l++;
    chomp;
    /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*\t[^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)\t([^\t]*)$/ || die("Bad format in input file: $_\n");
    
    $forme=$4;
    $lemme=$5."\t".$7;
    $tag=$6;
    $attestedform=$1;
    $attestedlemma=$2;
    $forme=~s/\t//;
    if ($lemme=~/\t([^-]+)-/) {$cat=$1} elsif ($lemme=~/\t(.*)$/) {$cat=$1} else {die "Bad format in lemma \"$lemme\" (line $l: $_)\n";}
    $nombre_lemmes_possibles_sachant_forme_et_cat_et_tag_FCT{$forme}{$cat}{$tag}++;
}

print STDERR "\r  Parsing data file...done         \n  Output...";
for $forme (keys %nombre_lemmes_possibles_sachant_forme_et_cat_et_tag_FCT) {
    $ntot=0;
    for $cat (keys %{$nombre_lemmes_possibles_sachant_forme_et_cat_et_tag_FCT{$forme}}) {
	for $tag (keys %{$nombre_lemmes_possibles_sachant_forme_et_cat_et_tag_FCT{$forme}{$cat}}) {
	    $n=$nombre_lemmes_possibles_sachant_forme_et_cat_et_tag_FCT{$forme}{$cat}{$tag};
	    $ntot+=$n;
	    print "$forme\t$cat\t$tag\t$n\n";
	}
    }
    print "$forme\t__ALL__\t__ALL__\t$ntot\n";
}
print STDERR "done\n";
