$loccfile=shift;
$derfile=shift;

use utf8;
use open ':utf8';

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


open(DER,"<$derfile") || die("Der file $derfile not found\n");
my $dl;
while (<DER>) {
    chomp;
    /^([^\t]*\t[^\t]*)\t([^\t]*\t[^\t]*)$/ || die("Error in input format (file $derfile)\n");
    $ol=$1;
    $dl=$2;
    $dl=~s/(.*)_/$1/;
    $derlemma{$dl}=1;
    $orlemma{$ol}=1;
    $derivation{$ol}{$dl}=1;
}
open(LOCC,"<$loccfile") || die("Occs file $loccfile not found\n");
while (<LOCC>) {
    chomp;
    @line=split("\t",$_);
    $occ{$line[0]."\t".$line[1]}{$line[2]}+=$line[3];
    if ($line[2] ne "") {
	$occ{$line[0]."\t".$line[1]}{""}+=$line[3]/100;
    }
}
#print STDERR $occ{"mil\tadj-b"}{""}."\n";
for $lemma (keys %derivation) {
    if (defined($occ{$lemma})) {
	for $derlemma (keys %{$derivation{$lemma}}) {
	    if (defined($occ{$derlemma}{""})) {
		$occ{$lemma}{""}+=$occ{$derlemma}{""}/100;
	    }
	}
    }
}
#print STDERR $occ{"mil\tadj-b"}{""}."\n";
$currlemma="";
while (<>) {
    $linenumber++;
    if ($linenumber % 10000 == 0) {print STDERR "$linenumber\r";}
    chomp;
    @line=split("\t",$_);
    $lemma=$line[0]."\t".$line[3];
    $lemma=~s/:[^\t]*$//;
#    $form=$line[2];
#    $form=~s/^(.*)_/\1/;
    $tag=$line[2];
    if ($occ{$lemma}{""}>0) {
	if ($tag=~/D\[(.*)\]:/) {
	    if ($occ{$lemma}{$1}>0) {print $_."\n";}
	} elsif (defined($derlemma{$lemma})) {
	    print $_."\n";
	} elsif ($occ{$lemma}{""}>1 && $lemma!~/\t.*inv$/) {
	    print $_."\n";
	}
    }
}
