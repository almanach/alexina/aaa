#!/usr/bin/env perl

while(<>) {
    /^([^\t]*)\t([0-9]+)$/ || die "Error in input format";
    for $i (1..$2) {
	print $1."\n";
    }
}
