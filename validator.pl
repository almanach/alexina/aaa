#!/usr/bin/env perl

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $validatedlexiconfile=shift;
my $prevalidatedlexiconfile=shift;
my $printoriginal=0;
$printoriginal=shift;
open(FICH, $validatedlexiconfile) || die("$validatedlexiconfile not found $!\n"); #ouverture du fichier en lecture  
my (%validatedlemma, %prevalidatedlemma);
my $lemma;
my $conj;
my %orders;
my %original;
my %cpcache;
print STDERR "  Loading validated lexicon...";
my ($l,$on);
while (<FICH>) { 
    chomp; 
    if ($l++ % 1000 == 0) {print STDERR "\r  Loading validated lexicon...".($l-1)." / ".$on;}
    /^[^\t]*\t([^\t]*)\t([^\t]*)\t/ || /^([^\t]*)\t([^\t]*)$/ || die("Bad format in file $validatedlexiconfile: $_\n");
    my $lemma=$1;
    my $conj=$2;
    $validatedlemma{$lemma}{$conj}=1;
    if ($conj=~/^([^:]+)(:.*)$/) {
	%orders=();
	$conj=$1;
	if (!defined($cpcache{$2})) {
	  $cpcache{$2}->{""}=$2;
	  &compute_permutations($cpcache{$2},"");
	  $on++;
	  for (keys %{$cpcache{$2}}) {
	    $original{$conj.$_}=$conj.$2;
	  }
	}
    } else {
	$original{$conj}=$conj;
    }
}
close(FICH);
print STDERR "\r  Loading validated lexicon...done         \n";

if ($prevalidatedlexiconfile ne "" && $prevalidatedlexiconfile ne "NONE") {
    open(FICH, $prevalidatedlexiconfile) || die("$prevalidatedlexiconfile not found $!\n"); #ouverture du fichier en lecture  
    while (<FICH>) { 
	chomp; 
	/^[^\t]*\t([^\t]*)\t([^\t]*)\t/ || /^([^\t]*)\t([^\t]*)$/  || die("Bad format in file $prevalidatedlexiconfile: $_\n");
	my $lemma=$1;
	my $conj=$2;
	$prevalidatedlemma{$lemma}{$conj}=1;
	if ($conj=~/^([^:]+)(:.*)$/) {
	  %orders=();
	  $conj=$1;
	  if (!defined($cpcache{$2})) {
	    $cpcache{$2}->{""}=$2;
	    &compute_permutations($cpcache{$2},"");
	    for (keys %{$cpcache{$2}}) {
	      $original{$conj.$_}=$conj.$2;
	    }
	  }
	} else {
	  $original{$conj}=$conj;
	}
    }
    close(FICH);
}

my $lemme;

my $n=0;
while(<>) {
    if (/^([^\t]*)\t([^\t\n]*)/) {
	chomp;
	if ($n % 1000 ==0) {print STDERR "$n\r";}
	$n++;
	$lemme=$1;
	$conj=$original{$2} || $2;
	if (defined($validatedlemma{$lemme}{$conj})) {print "+";}
	elsif (defined($prevalidatedlemma{$lemme}{$conj})) {print "?";}
	print "$_";
	if ($printoriginal) {
	    if (defined($validatedlemma{$lemme}{$conj})) {print "\t+$lemme\t$conj";}
	    elsif (defined($prevalidatedlemma{$lemme}{$conj})) {print "\t?$lemme\t$conj";}
	    else {print "\t$lemme\t$conj";}
	}
	print "\n";
    } else {
	print "$_";
    }
}



sub compute_permutations {
    my $o=shift;
    my $prevp=shift;
    my $prevc=$o->{$prevp};
    my $newc;
    delete($o->{$prevp});
    if ($prevc=~/^:(.)/) {
	for (split(/:/,$prevc)) {
	    if (/./) {
		$newc=$prevc;
		$newc=~s/:$_(:|$)/$1/;
		$o->{$prevp.":".$_}=$newc;
		if ($newc ne "") {
		    &compute_permutations($o,$prevp.":".$_);
		}
	    }
	}
    } else {
	die("Erreur: $prevc\n");
    }
}
