#!/usr/bin/env perl

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $occcompfile=shift;
my %occ;

open(OCC,"< $occcompfile") || die("$occcompfile could not be opened\n");
while(<OCC>) {
    chomp;
    /^([^\t]*)\t([^\t]*)\t(.*)$/;
    $occ{$1}=$3;
}

my $lemme;
my $forme;
my $tag;
my $conj;
my $bconj;
my %locc;
my %seenformsfromlemma_LF;

my $l=0;

while(<>) {
    $l++;
    if ($l % 10000 == 0) {print STDERR "$l\r";}
    chomp;
    /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)$/;
    $lemme=$1;
    $forme=$2;
    $tag=$3;
    $conj=$4;
    $bconj=$conj;
    $bconj=~s/:.*//;
    $forme=~s/^(.*)_/$1/;
    if (!defined($seenformsfromlemma_LF{$lemme."\t".$bconj}{$forme})) {
	if ($tag=~/^D\[(.*)\]:.*$/) {
	    $locc{$lemme."\t".$bconj}{$1}+=$occ{$forme};
	} else {
	    $locc{$lemme."\t".$bconj}{""}+=$occ{$forme};
	}
	$seenformsfromlemma_LF{$lemme."\t".$bconj}{$forme}=1;	
    }
}

for my $l (keys %locc) {
    for (keys %{$locc{$l}}) {
	print "$l\t$_\t$locc{$l}{$_}\n";
    }
}
