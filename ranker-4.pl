# emploi typique:
# perl ranker-3.pl mondediplo-verbe.ok mondediplo-verbe.occcomp mondediplo-verbe.nlf

use strict;

my $arg=$ARGV[0];
$arg=~s/^(.*)\.[^\.]*$/$1/; 
open(LOG, "> $arg.trace") || die("$!\n"); #ouverture du fichier en lecture  

my $stepmax=10;

my $prefixes="(ne|naj)?(u|na|za|z|s|pre|pri|vy|od|do|ne)";

# charger les pr�fixes
my $arg="";
$arg="prefs";
#print STDERR $arg, "\n";
my %probapref=();
my %repref=();
if ($arg ne "") {
    print STDERR "Charge les pr�fixes et les informations y aff�rant...";
    open(FICH, $arg) || die("$arg non trouv� $!\n"); #ouverture du fichier en lecture  
    while (<FICH>) 
    { 
	chomp($_); 
	$_ =~ /^([^\t]*)\t([^\t]*)\t(.*)$/;
	$repref{$1}=$2;
	$probapref{$1}=$3;
    }
    close(FICH);
    print STDERR "done\n";
}

print STDERR "Charge les formes et les occurrences d'icelles...";
# charger les formes et occurences d'icelles
my $arg=$ARGV[1]; 
#print STDERR $arg, "\n";
open(FICH, $arg) || die("$arg non trouv� $!\n"); #ouverture du fichier en lecture  
my %occurrences=();
my %occurrencestotales=();
while (<FICH>) 
{ 
    chomp($_); 
    $_ =~ /^([^\t]*)\t([^\t]*)\t(.*)$/;
    $occurrences{$1}=$3;
    $occurrencestotales{$1}=max($3,$2);
    $occurrences{$1."_"}=$3;
    $occurrencestotales{$1."_"}=max($3,$2);
}
close(FICH);
print STDERR "done\n";

print STDERR "Charge le nombre de lemmes envisageables par forme...";
# charger le nombre de lemme pouvant generer chaque forme (fichier *.nlf)
my $arg=$ARGV[2]; 
open(FICH, $arg) || die("fichier non trouv� $!\n"); #ouverture du fichier en lecture  
my %nbrelf=();
while (<FICH>) 
{ 
     chomp($_); 
     $_ =~ /^([^\t]*)\t([^\t]*)\t(.*)/;
     $nbrelf{$1}=$2;
}
close(FICH);
print STDERR "done\n";


my $probainitiale=0.5;
# charger le formes et occurences d'icelles
my $arg=$ARGV[0]; 
#print STDERR $arg, "\n";
open(FICH, $arg) || die("$arg non trouv� $!\n"); #ouverture du fichier en lecture  
my %lemmesformes=();
my %formeslemmes=();
my %lemmes=();
my %decoupages=();
my %occ=(); #occ des lemmes
my $nonp=0;
my $l="";
my $f="";
my $pref="";
my $rad="";
my $count=0;
print STDERR "Chargement des lemmes...";
while (<FICH>) 
{ 
#    $count++;
#    if ($count % 1000 == 0) {print STDERR "$count\r";}
    chomp($_); 
    $_ =~ /^[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)\t([^\t]*)$/;
    $l=$3."\t".$5;
    $f=$1.$2;
    $lemmes{$l}=$probainitiale;
    if ($occurrences{$f}>0 && $nbrelf{$f}>0) {
	$occ{$l}+=$probainitiale*$occurrences{$f}/$nbrelf{$f};
	$occ{$l."_"}+=$probainitiale*$occurrences{$f}/$nbrelf{$f};
    }
}
close(FICH);
print STDERR "done\n";
print STDERR "Recherche de pr�fixes dans les lemmes...";
foreach $l (keys %lemmes) {
    foreach $pref (keys %repref) {
	if ($l=~/^$pref($repref{$pref}.*)$/) {
	    $rad=$1;
	    if ($lemmes{$rad}>0) {
		print STDERR "Je rajoute au lemme $l l'hypoth�se d'un lemme � pr�fixe $pref-$rad\n";
		$lemmes{$pref."#".$rad}=$probainitiale;
		$occ{$pref."#".$rad}=$occ{$l};
		$decoupages{$pref.$rad}{$pref."#".$rad}=1;
	    }
	}
    }
}
print STDERR "done\n";

my $rad="";
my $term="";
my $lemme="";
my $lemmecomplet="";
my $formname="";
my $conj="";
my $pos="";
my $forme="";
my $ending="";
my $proba=0;
my $contrib=0;
my $count=0;

my $fl=0;
my $pl="";
my $odd=1;
my $denominateur=1;
my $autrelemme="";
my $flproba=-1;
my $step=0;

my %oldlemmes=();

sub max{
    if (@_[0] > @_[1]) {
	return @_[0];
    } else {
	return @_[1];
    }
}

sub min{
    if (@_[0] < @_[1]) {
	return @_[0];
    } else {
	return @_[1];
    }
}

my $fl=0;
my $c="";
my $nf="";
my $pref="";
my $infiniteodd=0;
my $alreadyinfiniteodd=0;
my $contrib=0;
my %oldocc=();
my %probaformconj=();
my %nombreformesparterm=();
my %termdeform=();


for ($step=1; $step<=$stepmax; $step++) {
    print STDERR "Initialisation des distributions des formes pour chaque conjugaison...";
    open(FICH, $arg) || die("$arg non trouv� $!\n"); #ouverture du fichier en lecture  
    %probaformconj=();
    %nombreformesparterm=();
    %termdeform=();
    while (<FICH>) { 
	chomp($_); 
	$_ =~ /^[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)\t([^\t]*)$/ || die("Erreur - ligne:\n|$_|\n");
#	if ($2!~/_$/) {
	if (!defined($probaformconj{$5}{$4}) || !defined($nombreformesparterm{$5}{$2})) {$nombreformesparterm{$5}{$2}++;}
	$probaformconj{$5}{$4}+=($occurrences{$1.$2})*$lemmes{$3."\t".$5};
	$probaformconj{$5}{"total"}+=($occurrences{$1.$2})*$lemmes{$3."\t".$5};
	$termdeform{$5}{$4}=$2;
#	}
    }
    close(FICH);
    my $conj="";
    foreach $conj (keys %probaformconj) {
	foreach (keys %{$probaformconj{$conj}}) {
	    if ($_ ne "total" && $probaformconj{$conj}{$_}>0) {
		print STDERR "\$probaformconj{$conj}{$_}(=$probaformconj{$conj}{$_})/=$probaformconj{$conj}{total}*$nombreformesparterm{$conj}{$termdeform{$conj}{$_}}  - $termdeform{$conj}{$_}  \n";
		$probaformconj{$conj}{$_}/=$probaformconj{$conj}{total}*$nombreformesparterm{$conj}{$termdeform{$conj}{$_}};
	    }
	}
    }
    print STDERR "done\n";
    print STDERR "Charge les couples lemmes-formes possibles...";
    open(FICH, $arg) || die("$arg non trouv� $!\n"); #ouverture du fichier en lecture  
    while (<FICH>) { 
	chomp; 
	$_ =~ /^[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)\t([^\t]*)$/ || die("Erreur - ligne:\n$_\n");
	$l=$3."\t".$5;
	$f=$1.$2;
	$c=$5;
	$nf=$4;
	$nonp=min((1-$probaformconj{$c}{$nf}) ** max(1,$occ{$l}),0.9); # proba (locale) qu'aucune forme de tag $nf de $l ne soit attest�e parmi ses $occ{$l} occs
	if ($occurrences{$f}>0 && $occurrences{$f}/$occurrencestotales{$f}>=0.1) {
	    $lemmesformes{$l}{$f}=min(0.99,1-(1-$lemmesformes{$l}{$f})*$nonp); # proba qu'une forme $f d�e � $l soit attest�e si $l l'est
	} elsif ($occurrences{$f}>0 && $occurrences{$f}/$occurrencestotales{$f}<0.2){
	    $lemmesformes{$l}{$f}=-min(0.99,1-(1-(-$lemmesformes{$l}{$f}))*$nonp);
	    print STDERR "Forme $f rejet�e car trop rarement tagg�e selon la POS concern�e.\n ";
	} else {
	    $lemmesformes{$l}{$f}=-min(0.99,1-(1-(-$lemmesformes{$l}{$f}))*$nonp);
	}
	$formeslemmes{$f}{$l}=min(0.99,1-(1-$formeslemmes{$f}{$l})*$nonp); # proba qu'une forme $f d�e � $l soit attest�e si $l l'est
	if ($f=~/obmedzi/) {
	    print STDERR "\t\$lemmesformes{$l}{$f}=$lemmesformes{$l}{$f}\t$nonp\t$probaformconj{$c}{$nf}\t($nombreformesparterm{$c}{$termdeform{$c}{$nf}})\n";
	}
	foreach $pl (keys %{$decoupages{$l}}) {
	    $pl=~/^(.*)\#(.*)$/;
	    $pref=$1;
	    $rad=$2;
	    $nonp=min((1-$probaformconj{$c}{$nf}) ** max(1,$occ{$pl}),0.9);
	    if ($occurrences{$f}>1) {
		$lemmesformes{$pl}{$f}=min(0.99,1-(1-$lemmesformes{$pl}{$f})*$nonp);
	    } elsif ($occurrences{$f}==1 && $f=~/^$pref(.*)$/ && $occurrences{$1} > -1) {
		$lemmesformes{$pl}{$f}=min(0.99,1-(1-$lemmesformes{$pl}{$f})*$nonp);
#		    print STDERR "Forme $f accept�e pour le lemme � pr�fixe $pl malgr� une occurrence �gale � 1, car reconnue comme $pref-$1, o� $1 occure $occurrences{$1} fois.\n";
	    } else {
		$lemmesformes{$pl}{$f}=-min(0.99,1-(1-(-$lemmesformes{$pl}{$f}))*$nonp);
	    }
	    $formeslemmes{$f}{$pl}=min(0.99,1-(1-$formeslemmes{$f}{$pl})*$nonp);
	}
#    print STDERR "P($1$2|$l)=$lemmesformes{$l}{$1.$2}\n";
    }
    close(FICH);
    print STDERR "done\n";


    foreach $lemme (keys %lemmes) {
	$oldlemmes{$lemme}=$lemmes{$lemme}; # proba que $l soit attest� (et donc bon)
	$oldocc{$lemme}=$occ{$lemme};
    }
    print STDERR "---Etape $step...\n";
    print STDERR "Ranking des lemmes...";
    foreach $lemme (keys %lemmes) {
	$infiniteodd=0;
	$alreadyinfiniteodd=0;
	print STDERR "($step->)\t$lemme\t$lemmes{$lemme}\t$occ{$lemme}\n";
	if ($lemmes{$lemme}<1) {$odd=$lemmes{$lemme}/(1-$lemmes{$lemme});} else {$odd=100000000000; $infiniteodd=1; $alreadyinfiniteodd=1;}
	$occ{$lemme}=0;
      LEMME: foreach $forme (keys %{$lemmesformes{$lemme}}) {
#	    $fl=$lemmesformes{$lemme}{$forme};
	    $fl=$lemmesformes{$lemme}{$forme}*$oldlemmes{$lemme};
	    $denominateur=1;
	    foreach $autrelemme (keys %{$formeslemmes{$forme}}) {
		$l=$lemme; $l=~s/\#//;
		if ($autrelemme ne $l && $autrelemme !~ /\#/) {
		    $denominateur*=1-$formeslemmes{$forme}{$autrelemme}*$oldlemmes{$autrelemme}; # proba qu'une forme $f ne soit pas d�e � un autre lemme que $l
		}
	    }
	    if ($forme=~/^(.*)_$/) {
#		foreach $autrelemme (keys %{$formeslemmes{$1}}) {
#		    $l=$lemme; $l=~s/\#//;
#		    if ($autrelemme ne $l && $autrelemme !~ /\#/) {
#			$denominateur*=1-$formeslemmes{$1}{$autrelemme}*$oldlemmes{$autrelemme};
#		    }
#		}
	    } else {
		foreach $autrelemme (keys %{$formeslemmes{$forme."_"}}) {
		    $l=$lemme; $l=~s/\#//;
		    if ($autrelemme ne $l && $autrelemme !~ /\#/) {
			$denominateur*=1-$formeslemmes{$forme."_"}{$autrelemme}*$oldlemmes{$autrelemme};
		    }
		}
	    }
	    if ($odd<0) {
		$odd=0;
	    } elsif ($odd>0) {
		if ($infiniteodd && $forme!~/_/) { # lemme d�j� certain
		    if ($alreadyinfiniteodd==1) {print STDERR "($step)\t$lemme\td�j� certain\n"; $alreadyinfiniteodd=0}
		} elsif (($denominateur == 1 && $fl>0 && $odd>0) || $infiniteodd) { # le lemme pourrait devenir certain gr�ce � $forme 
                                                                                    # ($lemme semble �tre sa seule explication cr�dible � ce stade)
		    if (($forme!~/_$/) && (($occurrences{$forme}*$oldlemmes{$lemme}>0.05*$oldocc{$lemme} && $occurrences{$forme}*$oldlemmes{$lemme}<1+0.9*$oldocc{$lemme}) || ($lemme=~/\#/))) { # le lemme est d�sormais certain gr�ce � $forme
			if ($lemme =~ /^.*\t/ && $infiniteodd==0) {
			    print STDERR "($step)\t$lemme\t(oldlemmes=$oldlemmes{$lemme}, oldocc=$oldocc{$lemme})\t-> certain\t$forme\t($occurrences{$forme})\t(".$occurrences{$forme}*$oldlemmes{$lemme}."> 0.05 * ".$oldocc{$lemme}.")\todd=infinity\n";
			    $l=$lemme; $l=~s/\#//; print LOG "$l\t$forme\n";
			}
			$odd= 1000000000000000000;
			$infiniteodd=1;
		    } elsif ($forme=~/_/ && $occurrences{$forme}>0) { # le lemme est dit plausible � cause de la forme d�riv�e $forme
			print STDERR "($step)\t$lemme\t(oldlemmes=$oldlemmes{$lemme}, oldocc=$oldocc{$lemme})\t-> plausible\t$forme\t($occurrences{$forme})\todd*=".(1+$occurrences{$forme})."\n";
			print STDERR "\$occurrences{$forme}=$occurrences{$forme}\n";
			$odd*=log($occurrences{$forme})+1;
#			$odd*=$occurrences{$forme};
			
		    } else { # le lemme n'est pas promu au rang de certain, car $forme n'est pas assez attest�e
			print STDERR "($step)\t$lemme\t$forme : forme discriminante mais ne satisfait pas les crit�res permettant de rendre un lemme certain\todd*=".(1+$occurrences{$forme})."\n";
			$odd*=log($occurrences{$forme}+1);
#			$odd*=$occurrences{$forme};
		    }
		} else { # le lemme �volue normalement (aucune certitude potentielle)
		    if ($fl>0) { # $forme contribue par sa pr�sence
			$contrib=(1-$denominateur*(1-$fl))/((1-$denominateur));
			if ($forme=~/_$/) {
			    if ($contrib<1) {
				$contrib=1;
			    } else {
				$contrib=(1-$denominateur*(1-$fl))/((1-$denominateur));
				print STDERR "($step)\t$lemme\t$forme+\todd*=(1-(1-$fl)*$denominateur)/".(1-$denominateur)."\t=".$contrib."\t-> ";
			    }
			} else {
			    print STDERR "($step)\t$lemme\t$forme+\todd*=(1-(1-$fl)*$denominateur)/".(1-$denominateur)."\t=".$contrib."\t-> ";
			}
		    } elsif ($forme!~/_/) { # $forme contribue par son absence
			$contrib=1+$fl;
			print STDERR "($step)\t$lemme\t$forme-\todd*=".$contrib."*p/p\t=".$contrib."\t-> ";
		    } else {
			$contrib=1;
		    }
		    $odd*=$contrib;
		    print STDERR "odd=$odd\n";
		}
	    }
	    $occ{$lemme}=$occ{$lemme}+($occurrences{$forme})*$oldlemmes{$lemme};
	}
	if ($lemme =~ /^(.*)\#(.*)$/ && $oldlemmes{$2} > 0 && $infiniteodd==0) {
	    $pref=$1;
	    $rad=$2;
	    if ($probapref{$pref}*$oldlemmes{$rad} < 1) {
		$odd *= (1-$probapref{$pref}*(1-$oldlemmes{$rad}))/(1-$probapref{$pref}*$oldlemmes{$rad});
		print STDERR "($step)\t$pref-$rad est un lemme � pr�fixe : odd (=$odd) *= ".((1-$probapref{$pref}*(1-$oldlemmes{$rad}))/(1-$probapref{$pref}*$oldlemmes{$rad}))."\t$lemmes{$lemme}\t$occ{$lemme}\n";
	    } else {
		$odd=1000000000000;
		$infiniteodd=1;
		print STDERR "($step)\t$pref-$rad est un lemme � pr�fixe : odd *= +infini\t$lemmes{$lemme}\t$occ{$lemme}\n";
	    }
	}
	if ($infiniteodd) {$lemmes{$lemme}=1;} else {$lemmes{$lemme}=$odd/(1+$odd);}
	print STDERR "(<-$step)\t$lemme\t$lemmes{$lemme}\t$occ{$lemme}\n";
    }
    foreach $lemme (keys %lemmes) {
	foreach $forme (keys %{$lemmesformes{$lemme}}) {
	    $lemmesformes{$lemme}{$forme}=0;
	    $formeslemmes{$forme}{$lemme}=0;
	    if ($lemme =~ /^(.*)\#(.*)$/ && $oldlemmes{$2} > 0) {
		$pref=$1;
		$rad=2;
		if ($lemmes{$lemme}>$lemmes{$pref.$rad}) {
		    $lemmes{$pref.$rad}=$lemmes{$lemme};#0;
		    $oldlemmes{$pref.$rad}=$oldlemmes{$lemme};#=0;
		    $occ{$pref.$rad}=$occ{$lemme};#0};
		}
            }
        }
    }
    print STDERR "done\n";
}
print STDERR "---Normalisation des rankings finaux des lemmes...";
my %lemmesprefs=();
my %lemmesder=();
my $tmpint=0;
foreach $lemme (keys %lemmes) {
    if ($lemme=~/^(.*)\#(.*)$/) {
	$pref=$1;
	$rad=$2;
	$l=$pref.$rad;
	if ($lemmes{$l}<=$lemmes{$lemme}) {
	    $lemmes{$l}=$lemmes{$lemme};
	    $lemmesprefs{$l}=$pref;
	    $tmpint=$occ{$l};
	    $occ{$l}*=1+$occ{$rad}*$probapref{$pref}/5;
	    $occ{$rad}*=1+$tmpint*$probapref{$pref}/5;
	    print STDERR "Analyse de $l comme $lemmesprefs{$l}-$rad\n";
	}
    }
}
foreach $lemme (keys %lemmes) {
    print STDERR "Etude des d�riv�s de $lemme\n";
    foreach $forme (keys %{$lemmesformes{$lemme}}) {
	if ($forme=~/^(.*)_(.*)$/ && $occurrences{$forme}>0) {
	    if ($lemmesder{$lemme} ne "") {$lemmesder{$lemme}=$lemmesder{$lemme}." ";}
	    $lemmesder{$lemme}=$lemmesder{$lemme}.$1."(".$2." - ".$occurrences{$forme}.")";
	}
    }
}
foreach $lemme(keys %lemmes) {
    if (1000*$lemmes{$lemme}<1) {$lemmes{$lemme}=0;}
}
foreach $lemme (keys %lemmes) {
    if ($lemme !~ /\#/) {
	print $lemme."\t".$lemmesprefs{$lemme}."-\t".$lemmes{$lemme}."\t";#((log($lemmes{$lemme}+1)+100)/100)."\n";
	if ($lemmes{$lemme}<1) {$lemmes{$lemme}*=0.5;}
	$lemmes{$lemme}=$lemmes{$lemme}*(1+log(1+log(1+$occ{$lemme}))/1000);
	print $occ{$lemme}."\t".$lemmes{$lemme}."\t".$lemmesder{$lemme}."\n";#((log($lemmes{$lemme}+1)+100)/100)."\n";
    }
}
close(LOG);
exit(0);
