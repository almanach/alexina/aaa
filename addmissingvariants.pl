#!/usr/bin/env perl

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my %vars_given_conj_CV;
my %lemmaforms_given_conj_var_tag_CTVF;
my $lemma; my $form; my $tag; my $conj; my $prevlemma; my $var;
my @line;
my $l;
my %stoplemma; my %orders;
my %sortvlist_cache;

my $stoplemmesfile=shift;
open(STOPLEMMES,"<$stoplemmesfile") || die("Stoplemmes file $stoplemmesfile not found\n");
while (<STOPLEMMES>) {
    chomp;
    /^(.*)\t(.*)$/;
    $lemma=$1;
    $conj=$2;
    if ($conj=~/^([^:]+)(:.*)$/) {
	%orders=();
	$conj=$1;
	$orders{""}=$2;
	&compute_permutations(\%orders,"");
	for (keys %orders) {
	    $stoplemma{$lemma."\t".$conj.$_}=1;
	}
    } else {
	$stoplemma{$lemma."\t".$conj}=1;
    }
}

my %vars_given_conj_and_tag_CTV;
my $morphorad=shift;
open(MORPHODATA,"<$morphorad.data") || die("Morphological data file $morphorad.data not found\n");
while (<MORPHODATA>) {
    chomp;
    next if (/^\#\#EXCL\#\#\t/);
    /^([^\t]*)\t([^\t]*)\t([^\t]*)$/ || die("Bad format in morphological data file $morphorad.data: $_\n");
    if ($3 eq "") {
	$vars_given_conj_and_tag_CTV{$1}{$2}{$3}=1;
    } else {
	$vars_given_conj_and_tag_CTV{$1}{$2}{":".$3}=1;
    }
}
my %temp_vars_given_conj_CV; my $hasvars; my $newvar;
for my $table (keys %vars_given_conj_and_tag_CTV) {
    $vars_given_conj_CV{$table}{""}="";
    for my $tag (keys %{$vars_given_conj_and_tag_CTV{$table}}) {
#	print STDERR "$table\t[".join("  ",keys %{$vars_given_conj_CV{$table}})."]\n";
	$hasvars=0;
	%temp_vars_given_conj_CV=();
	for my $variant (keys %{$vars_given_conj_and_tag_CTV{$table}{$tag}}) {
	    if ($variant ne "") {
		$hasvars=1;
		for my $v (keys %{$vars_given_conj_CV{$table}}) {
		    if ($vars_given_conj_CV{$table}{$v}!~/$variant(:|$)/) {
			if ($v!~/$variant(:|$)/) {
			    $newvar=$v.$variant;
			} else {
			    $newvar=$v;
			}
			$temp_vars_given_conj_CV{$newvar}=$vars_given_conj_CV{$table}{$v};
			for my $variant2 (keys %{$vars_given_conj_and_tag_CTV{$table}{$tag}}) {
			    if ($variant ne $variant2) {
				if ($vars_given_conj_CV{$table}{$v}!~/$variant2(:|$)/) {
				    if ($v=~/$variant2(:|$)/) {
#					print STDERR "/$table/\tdeleting $newvar because of $variant2 ($vars_given_conj_CV{$table}{$v})\n";
					delete($temp_vars_given_conj_CV{$newvar});
					last;
				    } else {
					$temp_vars_given_conj_CV{$newvar}.=$variant2;
				    }
				}
			    }
			}
#			if (defined($temp_vars_given_conj_CV{$newvar})) {
#			    print STDERR "/$table/\t\$temp_vars_given_conj_CV{$newvar}=".$temp_vars_given_conj_CV{$newvar}."\n";
#			}
		    }
		}
	    }
	}
	if ($hasvars) {
	    %{$vars_given_conj_CV{$table}}=%temp_vars_given_conj_CV;
	}
    }
}
#for my $c (keys %vars_given_conj_CV) {
#    print STDERR "$c\t".join("  ",keys %{$vars_given_conj_CV{$c}})."\n";
#}

require("$morphorad.check.pl");

# L'entr�e doit �tre tri�e par lemmes
while (<>) {
    if (++$l % 10000 == 0) {print STDERR "$l\r";}
    chomp;
    $prevlemma=$lemma;
    @line=split(/\t/,$_);
    $lemma=$line[0];
    $form=$line[1];
    $tag=$line[2];
    $conj=$line[3];
    if ($conj=~s/:(.*)//) {$var=$1;} else {$var=""}
    if ($lemma ne $prevlemma) {
#	&add_conj_with_mutiple_variants();
	&output();
#	%vars_given_conj_CV=();
	%lemmaforms_given_conj_var_tag_CTVF=();
    }
#    $vars_given_conj_CV{$conj}{$var}=1;
    $lemmaforms_given_conj_var_tag_CTVF{$conj}{$tag}{$var}{$lemma."\t".$form}=1;
}
#&add_conj_with_mutiple_variants();
&output();

sub add_conj_with_mutiple_variants {
    for my $c (keys %vars_given_conj_CV) {
#	print STDERR "### $c ### ".$vars_given_conj_CV{$c}{""}." ###\n";
	my %old_vars=();
	my %vars=();
	$vars{""}=1;
	for my $v (keys %{$vars_given_conj_CV{$c}}) {
	    %old_vars=%vars;
	    %vars=();
	    for (keys %old_vars) {
		$vars{$_}=1;
		if ($v=~/./) {
		    $vars{$_.":".$v}=1;
		    delete($vars{""});
		}
	    }
	}
#	print STDERR "<".join(" ; ",keys %vars).">\n";
	my %bad_vars=();
	my %bad_vars_by_tag=();
	for my $t (keys %{$lemmaforms_given_conj_var_tag_CTVF{$c}}) {
#	    print STDERR "\t$t\t";
	    my %tmp_hash=();
	    $bad_vars_by_tag{$t}{""}=1;
	    for my $v (keys %{$lemmaforms_given_conj_var_tag_CTVF{$c}{$t}}) {
#		print STDERR "($v) ";
		%tmp_hash=%{$bad_vars_by_tag{$t}};
		%bad_vars_by_tag=();
		for (keys %tmp_hash) {
		    $bad_vars_by_tag{$t}{$_}=1;
		    if ($v ne "" && $_!~/:$v(:|$)/) {
			$bad_vars_by_tag{$t}{$_.":".$v}=1;
			$bad_vars_by_tag{$t}{":".$v.$_}=1;
		    }
		}
	    }
	    for (keys %{$bad_vars_by_tag{$t}}) {
		if (/^:[^:]+:[^:]+$/) {
		    $bad_vars{$_}=1; 
#		    print STDERR " [$_]";
		}
	    }
#	    print STDERR "\n";
	}
	for (keys %bad_vars) {
	    /^(:[^:]+)(:[^:]+)$/;
	    my $un=$1;
	    my $deux=$2;
	    for my $v (keys %vars) {
#		print STDERR "$v --- $un $deux --- ".($v=~/$un(:[^:]+)*$deux(:|$)/)."\n";
		if ($v=~/$un(:[^:]+)*$deux(:|$)/) {
		    delete($vars{$v});
		}
	    }
	}
#	print STDERR "<<".join(" ; ",keys %vars).">>\n";
	%{$vars_given_conj_CV{$c}}=%vars;
    }
}

sub output {
    my $printed;
    my $temp;
    for my $c (keys %vars_given_conj_CV) {
	for my $t (keys %{$lemmaforms_given_conj_var_tag_CTVF{$c}}) {
	    for my $v (keys %{$vars_given_conj_CV{$c}}) {
		if ($v eq "") {
		    if (!defined($stoplemma{$prevlemma."\t".$c}) && &check_lemma(&translitterate($prevlemma)."\t".$c.$v)) {
			for my $lf (keys %{$lemmaforms_given_conj_var_tag_CTVF{$c}{$t}{""}}) {
			    print "$lf\t$t\t$c\n";
			}
		    }
		} else {
		    $printed=0;
		    $temp=$v;
		    while ($temp=~s/:([^:]+)//) {
			if (defined($lemmaforms_given_conj_var_tag_CTVF{$c}{$t}{$1})) {
			    if (!defined($stoplemma{$prevlemma."\t".$c.$v}) && &check_lemma(&translitterate($prevlemma)."\t".$c.$v)) {
				for my $lf (keys %{$lemmaforms_given_conj_var_tag_CTVF{$c}{$t}{$1}}) {
				    print "$lf\t$t\t$c".&sortvlist($v)."\n";
				}
			    }
			    $printed=1;
			    $temp="";
			    last;
			}
		    }
		    if ($temp ne "") {die("### ERROR : inconsistency detected in subroutine \"output\" (\$temp=$temp)\n");}
		    if (!$printed) {
			for my $lf (keys %{$lemmaforms_given_conj_var_tag_CTVF{$c}{$t}{""}}) {
			    if (!defined($stoplemma{$prevlemma."\t".$c.$v}) && &check_lemma(&translitterate($prevlemma)."\t".$c.$v)) {
				print "$lf\t$t\t$c".&sortvlist($v)."\n";
			    }
			}
		    }
		}
	    }
	}
    }
}

sub compute_permutations {
    my $o=shift;
    my $prevp=shift;
    my $prevc=$o->{$prevp};
    my $newc;
    delete($o->{$prevp});
    if ($prevc=~/^:(.)/) {
	for (split(/:/,$prevc)) {
	    if (/./) {
		$newc=$prevc;
		$newc=~s/:$_(:|$)/$1/;
		$o->{$prevp.":".$_}=$newc;
		if ($newc ne "") {
		    &compute_permutations($o,$prevp.":".$_);
		}
	    }
	}
    } else {
	die("Erreur: $prevc\n");
    }
}
sub sortvlist {
    my $vl=shift;
    if (defined($sortvlist_cache{$vl})) {return $sortvlist_cache{$vl}}
    my $origvl=$vl;
    $vl=~s/^://;
    $vl=":".join(":",sort split(/\:/,$vl));
    while ($vl=~s/(:[^:]+)\1/\1/) {}
    $sortvlist_cache{$origvl}=$vl;
    return $vl;
}
