while (<>) {
    chomp;
    @l=split(/\t/,$_);
    $t=$l[3]."\t".$l[2];
    $t=~s/(\t[^-]+)-.*$/$1/;
    $t=~s/\t/-/;
    $t=~s/^-//;
    $t=~s/^(.*)-([^-]+)$/$2-$1/;
    $tagproba{$l[0]}{$t}+=$l[4];
    $tagproba{$l[0]}{__ALL__}+=$l[4];
}
for $w (keys %tagproba) {
    for (keys %{$tagproba{$w}}) {
	if ($tagproba{$w}{$_}>0 && $_ ne "__ALL__") {
#	    print "$w\t$_\t".abs(-log($tagproba{$w}{$_}))."\n";
	    print "$w\t$_\t".($tagproba{$w}{$_}/$tagproba{$w}{__ALL__})."\n";
	}
    }
}
