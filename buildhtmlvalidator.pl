#!/usr/bin/env perl

use strict;

my $rad=shift;
my @line;

my $lemmasperpage=50;

my $i=0; my $previ; my $nl;
my $color;
my $ok;
my $isfirstpage=1;
my $pagechanged=0;
while(<>) {
    if ($nl % $lemmasperpage == 0 && $pagechanged==0) {
	if ($nl>0) {
	    print OUTPUT "<INPUT type=\"hidden\" name=\"isfirstpage\" value=\"$isfirstpage\">\n";
	    $isfirstpage=0;
	    &print_ending();
	    close(OUTPUT);
	    if ($nl/$lemmasperpage == 100) {exit(0);}
	}
	open(OUTPUT,">$rad-validation/$rad.".($nl/$lemmasperpage).".html") || die("Could not open $rad.".($nl/$lemmasperpage).".html\n");
	&print_beginning();
	$previ=$i;
	$pagechanged=1;
	print OUTPUT "<INPUT type=\"hidden\" name=\"firstlemma\" value=\"".($i+1)."\">\n";
    }
    chomp;
    @line=split(/\t/,$_);
    $line[2]=~s/^([^,]*)/<b>$1<\/b>/;
#    print OUTPUT "<td>$line[7]</td>\n";#
    if (s/^\+//) {
	$ok=2
    } elsif (s/^\?//) {
	$ok=1;
	$color=" bgcolor=\"lightgreen\"";
    } elsif (s/^[\-\*]//) {
	die("Validation is performed either by html pages or directly in the generated file, not both.\n");
    } else {
	$color="";
	$ok=0;
    }
    $i++;
    if ($ok<2)  {
	$nl++;
	$pagechanged=0;
	print OUTPUT "<tr>\n";
	print OUTPUT "<td bgcolor=\"green\"><INPUT type=\"radio\" name=\"$i\" value=\"+\"\n";
	if ($ok) {print OUTPUT " checked"}
	print OUTPUT"></td>\n";
	print OUTPUT "<td bgcolor=\"red\"><INPUT type=\"radio\" name=\"$i\" value=\"*\"></td>\n";
	print OUTPUT "<td bgcolor=\"grey\"><INPUT type=\"radio\" name=\"$i\" value=\"-\"></td>";
	print OUTPUT "<td bgcolor=\"black\"><INPUT type=\"radio\" name=\"$i\" value=\"!\"></td>";
	print OUTPUT "<td$color>$line[2]<INPUT type=\"hidden\" name=\"line$i\" value=\"$_\"></td>\n";
	print OUTPUT "<td$color>$line[1]</td>\n";
	print OUTPUT "</tr>\n";
    } else {
	print OUTPUT "<INPUT type=\"hidden\" name=\"line$i\" value=\"$_\">\n";
	print OUTPUT "<INPUT type=\"hidden\" name=\"$i\" value=\"+\">\n";
    }
}
&print_final_ending();
close(OUTPUT);

sub print_beginning {
    print OUTPUT <<FIN;
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Validation page</title>
    </head>
    <body>
    <form action="../cgi-bin/generatetextfile.pl" method="post">
    <table border="1" align="center">
FIN
}

sub print_ending {
    my $j=$nl / $lemmasperpage;
    my $k=$i-$previ;
    print OUTPUT <<FIN;
    </table>
    <INPUT type="hidden" name="nextpage" value="$rad-validation/$rad.$j.html">
    <INPUT type="hidden" name="nlemmas" value="$k">
    <center>
    <INPUT type="submit" value="Send and go to next page">
    </center>
    </form>
    </body>
    </html>
FIN
}

sub print_final_ending {
    print OUTPUT <<FIN;
    </table>
    <INPUT type="hidden" name="nlemmas" value="$i">
    <center>
    <INPUT type="submit" value="Send (this is the last page)">
    </center>
    </form>
    </body>
    </html>
FIN
}
