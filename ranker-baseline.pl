#!/usr/bin/env perl

use strict;

my $occfile=shift;
my $lexfile=shift;
my $blinfile=shift;

print STDERR "  Loading form occurrences...";
my %occ;
open(OCC,"< $occfile") || die("$occfile could not be opened\n");
while(<OCC>) {
    chomp;
    /^([^\t]*)\t([^\t]*)\t(.*)$/;
    $occ{$1}=$3;
}
close(OCC);
print STDERR "done\n";

my ($l,$f,$t,$ln,$lastln);
my %ltermocc; my %lterm_table_occ;
if ($lexfile ne "NONE") {
  print STDERR "  Building quantitative data from existing lexicon (.ls file)...";
  open(LEX,"< $lexfile") || die("$occfile could not be opened\n");
  while(<LEX>) {
    chomp;
    /^([^\t]*)\t([^\t]*).*/;
    $l=$1; $t=$2;
    $l=~s/^.*(....)$/\1/;
    $ltermocc{$l}++;
    $lterm_table_occ{$l."\t".$t}++;
  }
  close(LEX);
  print STDERR "done\n";
}

print STDERR "  Loading hypotheses file...";
open(BLIN,"< $blinfile") || die("$blinfile could not be opened\n");
my $n;
my (%forms_of_lemma,%lemmas_of_form,%occs_of_lemma,%lemma2lnum,%attformsnumber_of_lemma,%formcontrib_for_lemma);
my (@lnum2lemma,@attformsnumber_of_lemma,@formcontrib_for_lemma);
while(<BLIN>) {
    if ($n % 10000 == 0) {print STDERR "\r  Loading hypotheses file... $n";}
    $n++;
    chomp;
    /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)$/ || die("Bad format in input file: $_\n");
    $f=$1.$2;
    next if (!defined{$occ{$f}} || $occ{$f}==0);
    $l=$3."\t".$5;
    $t=$4;
    push(@{$forms_of_lemma{$l}{$f}},$t);
    if (!defined($lemmas_of_form{$f}{$l})) {
	$occs_of_lemma{$l}+=$occ{$f};
    }
    $attformsnumber_of_lemma{$l}+=1/(1+$lemmas_of_form{$f}{$l}*2);
    $formcontrib_for_lemma{$l}{$f}+=1/(1+$lemmas_of_form{$f}{$l}*2);
    $lemmas_of_form{$f}{$l}++;
}
print STDERR "\r  Loading hypotheses file... done         \n";

print STDERR "  Eliminating least probable hypothetical lemmas...";
my $el;
for $l (keys %attformsnumber_of_lemma) { 
# we eliminate lemmas attested only by 3 forms:
# they need another statistical (n-gram based?) technique
    if ($attformsnumber_of_lemma{$l} < 4) {
	$el++;
	delete($attformsnumber_of_lemma{$l});
	delete($forms_of_lemma{$l});
	delete($occs_of_lemma{$l});
	delete($lemmas_of_form{$f});
    } else {
	$lemma2lnum{$l}=$ln;
	$lnum2lemma[$ln]=$l;
	$attformsnumber_of_lemma[$ln]=$attformsnumber_of_lemma{$l};
	for $f (keys %{$forms_of_lemma{$l}}) {
	    $formcontrib_for_lemma[$ln]{$f}=$formcontrib_for_lemma{$l}{$f};
	}
	$ln++;
    }
}
close(BLIN);
print STDERR "$el eliminated, $#attformsnumber_of_lemma kept\n";

print STDERR "  Computing and outputing first baseline...";
$n=0; my %seenforms;
my $base;
while (($l = &get_best_lemma()) && $l ne "") {
    if ($n % 100 == 0) {print STDERR "\r  Computing and outputing first baseline... $n";}
    $n++;
    $l=~/^([^\t]+)\t/; $base=$1;
    $base=`echo "$base" | recode u8..l2 | sxspellerpol.out -mw 0 -lw 30 | recode l2..u8`;
    if ($base =~ /^<<</) {$base=""}
    print "#$l\t".$attformsnumber_of_lemma[$lemma2lnum{$l}]."\t".$occs_of_lemma{$l}."\t$base\n";
    %seenforms=();
    for $f (keys %{$forms_of_lemma{$l}}) {
	for $t (0..$#{$forms_of_lemma{$l}{$f}}) {
	    print "$f\t$l\t$forms_of_lemma{$l}{$f}[$t]\n";
	}
	for my $l2 (keys %{$lemmas_of_form{$f}}) {
	    if (!$seenforms{$f}) {
		$occs_of_lemma{$l2}-=$occ{$f};
	    }
	    $attformsnumber_of_lemma[$lemma2lnum{$l2}]-=$formcontrib_for_lemma[$lemma2lnum{$l2}]{$f};
#	    delete($formcontrib_for_lemma[$lemma2lnum{$l2}]{$f});
#	    delete($formcontrib_for_lemma{$l}{$f});
	}
	$seenforms{$f}=1;
    }
    delete($forms_of_lemma{$l});
    delete($occs_of_lemma{$l});
    delete($attformsnumber_of_lemma{$l});
    $attformsnumber_of_lemma[$lemma2lnum{$l}]=-1;
    delete($lemmas_of_form{$f});
}
print STDERR "\r  Computing and outputing first baseline... done          \n";

sub get_best_lemma {
    my $bestocc=0;
    my ($result,$resulttermtable);
    my ($possibleresult, $possibleresulttermtable);
    for (0..$#attformsnumber_of_lemma) {
	if ($attformsnumber_of_lemma[$_]>$bestocc) {
	    $result=$lnum2lemma[$_];
	    $resulttermtable=$result;
	    $resulttermtable=~s/^.*(....\t)/\1/;
	    $bestocc=$attformsnumber_of_lemma[$_];
	} elsif ($attformsnumber_of_lemma[$_] == $bestocc) {
	    $possibleresult=$lnum2lemma[$_];
	    $possibleresulttermtable=~s/^.*(....\t)/\1/;
	    if ($lterm_table_occ{$possibleresulttermtable} > $lterm_table_occ{$resulttermtable}) {
	      print STDERR "preferred \"$possibleresult\" to \"$result\" for statistical reasons\n";
	      $result=$possibleresult;
	      $resulttermtable=$possibleresulttermtable;
	    }
	}
    }
    return $result;
}
