#!/usr/bin/env perl

use strict;

my $maxproba;
my $besttag;
my $l;

while(<>) {
    if (($l++ + 1) % 1000 == 0) {print STDERR "$l\r";}
    $maxproba=0;
    $besttag="";
    chomp;
    /^([^\t]*)\t(.*)$/ || die("Bad format in input file: $_\n");
    print $1;
    for (split(/\|/,$2)) {
	/^([^\t]*\t[^\t]*\t[^\t]*)\t([^\t]*)$/ || die("Bad format in input file: $_\n");
	if ($2>$maxproba) {
	    $maxproba=$2;
	    $besttag=$1;
	}
    }
    $besttag=~s/\t([^\t]+)(-[^-\t]*(?:\[[^\t]*\])?)\t/\t$1$2\t$1-/ || $besttag=~s/^(.*)\t/$1\t$1-/;
    $besttag=~s/-$//;
    print "\t$besttag\n";
}
