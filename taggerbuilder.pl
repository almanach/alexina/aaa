#!/usr/bin/env perl

use strict;

my $rad=shift;
my $min=shift;
my $max=shift;
my $add_uncertainty=0;

my $command;

for my $i ($min..$max) {
    $add_uncertainty=($i==0);
    print STDERR "*** S T E P   $i ***\n";
    $command="cat $rad.$i.tagged | perl extract_bigrams.pl $rad.stoplist > $rad.$i.tagbigrams";
#    print STDERR "$command\n";
    `$command`;
    $command="cat $rad.$i.tagged | perl tagger.pl $rad.$i.tagbigrams $rad.stoplist $rad.forms $add_uncertainty > $rad.".($i+1).".tagged";
#    print STDERR "$command\n";
    `$command`;
}
