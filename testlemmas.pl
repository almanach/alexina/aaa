#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

require(shift);

while (<>) {
    chomp;
    print "$_\t".check_lemma(translitterate($_))."\n";
}
