<letterclass name="v" letters="a à â ä e é è ê ë i î ï o ô ö u û ü ù y ÿ"/>
<letterclass name="e" letters="e é"/>
<letterclass name="E" letters="a à â ä i î ï o ô ö u û ü ù y ÿ"/>
<letterclass name="o" letters="a à â ä o ô ö u û ü ù"/>
<letterclass name="u" letters="o u"/>
<letterclass name="c" letters="b c ch d f gu g h j k gl l m gn n ŋ p qu q tr br vr cr gr r s t v w x z"/>
<letterclass name="R" letters="a à â ä e é è ê ë i î ï o ô ö u û ü ù y ÿ b c ch d f gu g h j k gl l m gn n ŋ p qu q tr br vr cr gr s t v w x z"/>
<letterclass name="X" letters="b c ch d f gu g h j k gl l m gn n ŋ p qu q tr br vr cr gr r s t v z"/>
<letterclass name="U" letters="a à â ä b c ch d e é è ê ë f gu g h i î ï j k gl l m gn n ŋ o ô ö p qu q tr br vr cr gr r s t v w x y ÿ z"/>
<letterclass name="N" letters="a à â ä b c ch e é è ê ë f gu g h i î ï j k gl l m gn o ô ö q tr br vr cr gr s u û ü ù v w x ÿ z"/>
<letterclass name="T" letters="a à â ä b c ch d e é è ê ë f gu g h i î ï j k gl l m gn n ŋ o ô ö p qu q tr br vr cr gr r s u û ü ù v w x y ÿ z"/>
<letterclass name="h" letters="b c ch d f gu g h j k gl l m gn n ŋ p qu q s t v w x z"/>
<letterclass name="s" letters="s t"/>
<letterclass name="k" letters="t v"/>
<letterclass name="i" letters="i î"/>

<translitteration source="gu" target="ɠ"/>
<translitteration source="qu" target="ʠ"/>
<translitteration source="gl" target="ǵ"/>
<translitteration source="gn" target="ň"/>
<translitteration source="tr" target="ṭ"/>
<translitteration source="br" target="ḅ"/>
<translitteration source="vr" target="ṿ"/>
<translitteration source="cr" target="ċ"/>
<translitteration source="gr" target="ġ"/>
<translitteration source="ch" target="š"/>

<!-- fusions -->
<fusion source="g_\o" target="ge_\o"/>
<fusion source="c_\o" target="ç_\o"/>
<fusion source="qu_\c" target="c_\c"/>
<fusion source="qu_u" target="c_u" final="+"/>

<fusion source="i_^" target="î_"/>
<fusion source="ï_ions" target="y_ions"/>
<fusion source="ï_iez" target="y_iez"/>
<fusion source="ï_\i" target="ï_"/>
<fusion source="ï_\E" target="y_\E"/>
<fusion source="ï_ez" target="y_ez"/>
<fusion source="ï_-" target="i_"/>
<fusion source="oï_" target="ouï_"/>
<fusion source="ï_^" target="ï_"/>
<fusion source="ï_=" target="i_"/>

<!-- -ayer -> -aye ou -aie ; envoyer-->
<fusion source="y_-" target="i_" final="+"/>

<!-- venir, tenir, acquérir, *asseyir (asseoir: formes en ie/ey) et les autres -->
<fusion source="quér_\i\s$" target="qu_\i\s$" final="+"/>
<fusion source="quér_s" target="qu_is" final="+"/>
<fusion source="quér_\i\c\*" target="qu_\i\c\*" final="+"/>
<fusion source="sey_\i\s$" target="s_\i\s$" final="+"/>
<fusion source="sey_s" target="s_is" final="+"/>
<fusion source="sey_\i\c\*" target="s_\i\c\*" final="+"/>
<fusion source="sey_r$" target="se_oir$" final="+"/>

<fusion source="\ken_-" target="\kien_" final="+"/>
<fusion source="quér_-" target="quièr_" final="+"/>
<fusion source="mour_-" target="meur_" final="+"/>
<fusion source="sey_-\s" target="sied_\s" final="+"/>
<fusion source="sey_-r" target="sié_r" final="+"/>
<fusion source="\Uill_-s" target="\Uu_x" final="+"/>
<fusion source="\Uill_-t" target="\Uu_t" final="+"/>
<fusion source="uill_-\s" target="u_\s" final="+"/>
<fusion source="qu_-t$" target="c_$" final="+"/>
<fusion source="qu_-s$" target="c_s$" final="+"/>

<fusion source="ŋ_-r" target="ŋ_r" final="+"/>
<fusion source="n_-r" target="n_r" final="+"/>
<fusion source="nd_-r" target="nd_r" final="+"/>
<fusion source="mp_-r" target="mp_r" final="+"/>
<fusion source="tt_-r" target="tt_r" final="+"/>
<fusion source="qu_-r" target="c_r" final="+"/>
<fusion source="ill_-r" target="ill_er" final="+"/>
<fusion source="\cr_-r" target="\cr_ir" final="+"/>
<fusion source="\h_-r" target="\h_ir" final="+"/>
<fusion source="_-" target="_" final="+"/>

<fusion source="ŋ_r" target="nd_r" final="+"/>
<fusion source="n_r" target="nd_r" final="+"/>

<fusion source="\ken_\i\s$" target="\k\in_\s$" final="+"/>
<fusion source="\ken_\i\c\*" target="\k\in_\c\*" final="+"/>
<fusion source="\kien_e$" target="\kienn_e$" final="+"/>
<fusion source="\kien_es$" target="\kienn_es$" final="+"/>
<fusion source="\kien_ent$" target="\kienn_ent$" final="+"/>
<fusion source="pren_e$" target="prenn_e$" final="+"/>
<fusion source="pren_es$" target="prenn_es$" final="+"/>
<fusion source="pren_ent$" target="prenn_ent$" final="+"/>

<!--<fusion source="n_\s" target="nd_\s" final="+"/>-->
<fusion source="ŋ_\c" target="n_\c" final="+"/>
<fusion source="ŋ_\v" target="gn_\v" final="+"/>
<fusion source="d_t$" target="d_$" final="+"/>
<fusion source="tt_d" target="_t" final="+"/>
<fusion source="tt_t$" target="_t$" final="+"/>
<fusion source="tt_s$" target="_ts$" final="+"/>


<fusion source="\c_r$" target="\c_re$" final="+"/>

<!-- courbaturer / courbatu -->
<fusion source="r_<" target="_"/>

<!-- aller -->
<fusion source="all_<" target="_" final="+"/>

<!-- *seoir -->
<fusion source="soi_e<" target="seoi_" final="+"/>
<fusion source="soi_<\i" target="s_\i" final="+"/>
<fusion source="choi_<" target="ch_" final="+"/>

<fusion source="pren_<" target="pr_" final="+"/>
<fusion source="mett_<" target="m_" final="+"/>

<!-- ouvrir / ouvert -->
<fusion source="vr_e<" target="ver_" final="+"/>
<fusion source="ffr_e<" target="ffer_" final="+"/>

<!-- courir, cueillir -->
<fusion source="quièr_\c" target="quer_\c" final="+"/>

<!-- faillir, bouillir, dormir... -->
<fusion source="ill_\s" target="_\s" final="+"/>
<fusion source="t_\s" target="_\s" final="+"/>
<fusion source="dorm_\s" target="dor_\s" final="+"/>
<fusion source="serv_\s" target="ser_\s" final="+"/>

<fusion source="mour_<" target="m_" final="+"/>

<!-- acquérir -->
<fusion source="quér_<" target="qu_" final="+"/>

<!-- fuir, broyer -->
<fusion source="\ui_\i\c" target="\u\i_\c" final="+"/>
<fusion source="\ui_\i$" target="\u\i_$" final="+"/>
<fusion source="\ui_\ie$" target="\u\i_e$" final="+"/>
<fusion source="\ui_\ies$" target="\u\i_es$" final="+"/>
<fusion source="\ui_\v" target="\uy_\v" final="+"/>
<fusion source="\uy_e$" target="\ui_e$" final="+"/>
<fusion source="\uy_es$" target="\ui_es$" final="+"/>
<fusion source="\uy_er\*" target="\ui_er\*" final="+"/>
<fusion source="\uy_ent$" target="\ui_ent$" final="+"/>
<fusion source="\uy_ement" target="\ui_ement" final="+"/>

<fusion source="\e\X_e$" target="è\X_e$" final="+"/>
<fusion source="\e\X_es$" target="è\X_es$" final="+"/>
<fusion source="e\X_er\R" target="è\X_er\R" final="+"/>
<!-- pas de "é" pour le futur et le conditionnel, cf Bescherelle table 11 note 2-->
<fusion source="\e\X_ent$" target="è\X_ent$" final="+"/>
<fusion source="\e\X_ement" target="è\X_ement" final="+"/>

<fusion source="et:_e$" target="ett_e$" final="+"/>
<fusion source="et:_es$" target="ett_es$" final="+"/>
<fusion source="et:_er\*" target="ett_er\*" final="+"/>
<fusion source="et:_ent$" target="ett_ent$" final="+"/>
<fusion source="et:_ement" target="ett_ement" final="+"/>

<fusion source="el:_e$" target="ell_e$" final="+"/>
<fusion source="el:_es$" target="ell_es$" final="+"/>
<fusion source="el:_er\*" target="ell_er\*" final="+"/>
<fusion source="el:_ent$" target="ell_ent$" final="+"/>
<fusion source="el:_ement" target="ell_ement" final="+"/>


<fusion source="t:_" target="t_" final="+"/>
<fusion source="l:_" target="l_" final="+"/>

<!-- (dé|...)briefer ; envoyer -->
<fusion source="brièf_" target="brief_" final="+"/>
<fusion source="envoi_er\*" target="env_err\*" final="+"/>


<table name="tag_unknown" source="" rads="..*">
    <form target="" tag="-"/>
</table>
<!-- ADJECTIFS -->
<table name="adj-e2" source="e" rads="..*">
    <form target="e" tag="-s"/>
    <form target="es" tag="-p"/>
    <derivation target="ifier" table="v-er"/>
    <derivation target="iser" table="v-er"/>
</table>
<table name="adj-2" source="" rads="..*">
    <form target="" tag="-s"/>
    <form target="s" tag="-p"/>
    <derivation target="ifier" table="v-er"/>
    <derivation target="iser" table="v-er"/>
</table>
<table name="adj-ique2" source="que" rads="..*i">
    <form target="que" tag="-s"/>
    <form target="ques" tag="-p"/>
    <derivation target="fier" table="v-er"/>
    <derivation target="ser" table="v-er"/>
</table>
<!-- NOMS -->
<table name="nc-2" source="" rads="..*">
    <form target="" tag="-s"/>
    <form target="s" tag="-p"/>
</table>
<table name="nc-4" source="" rads=".*(\E|\c)">
    <form target="" tag="m-s"/>
    <form target="e" tag="f-s"/>
    <form target="s" tag="m-p"/>
    <form target="es" tag="f-p"/>
</table>
<table name="nc-teur" source="eur" rads="..*t">
    <form target="eur" tag="m-s"/>
    <form target="rice" tag="f-s"/>
    <form target="eurs" tag="m-p"/>
    <form target="rices" tag="f-p"/>
</table>
<table name="nc-eur" source="r" rads="..*eu">
    <form target="" tag="m-s"/>
    <form target="se" tag="f-s"/>
    <form target="rs" tag="m-p"/>
    <form target="ses" tag="f-p"/>
</table>
<table name="nc-et" source="" rads=".*et">
    <form target="" tag="m-s"/>
    <form target="te" tag="f-s"/>
    <form target="s" tag="m-p"/>
    <form target="tes" tag="f-p"/>
</table>
<!-- VERBES -->
<table name="v-er" source="er" rads="...*">
    <form target="er" tag="W" show="#"/>
    <form target="a" tag="J3s"/>
    <form target="ai" tag="J1s"/>
    <form target="aient" tag="I3p"/>
    <form target="ais" tag="I12s"/>
    <form target="ait" tag="I3s"/>
    <form target="ant" tag="G"/>
    <form target="as" tag="J2s"/>
    <form target="asse" tag="T1s"/>
    <form target="assent" tag="T3p"/>
    <form target="asses" tag="T2s"/>
    <form target="assiez" tag="T2p"/>
    <form target="assions" tag="T1p"/>
    <form target="e" tag="PS13s"/>
    <form target="e" tag="Y2s" show="#"/>
    <form target="ent" tag="PS3p"/>
    <form target="era" tag="F3s"/>
    <form target="erai" tag="F1s"/>
    <form target="eraient" tag="C3p"/>
    <form target="erais" tag="C12s"/>
    <form target="erait" tag="C3s"/>
    <form target="eras" tag="F2s"/>
    <form target="erez" tag="F2p"/>
    <form target="eriez" tag="C2p"/>
    <form target="erions" tag="C1p"/>
    <form target="erons" tag="F1p"/>
    <form target="eront" tag="F3p"/>
    <form target="es" tag="PS2s"/>
    <form target="ez" tag="Y2p"/>
    <form target="ez" tag="P2p"/>
    <form target="iez" tag="I2p"/>
    <form target="iez" tag="S2p"/>
    <form target="ions" tag="I1p"/>
    <form target="ions" tag="S1p"/>
    <form target="ons" tag="P1p" show="#"/>
    <form target="ons" tag="Y1p"/>
    <form target="âmes" tag="J1p"/>
    <form target="ât" tag="T3s"/>
    <form target="âtes" tag="J2p"/>
    <form target="èrent" tag="J3p"/>
    <form target="é" tag="Kms"/>
    <form target="ée" tag="Kfs"/>
    <form target="ées" tag="Kfp"/>
    <form target="és" tag="Kmp"/>
    <form target="-e" tag="PS13s" rads="..*ay"/>
    <form target="-e" tag="Y2s" rads="..*ay" show="#"/>
    <form target="-ent" tag="PS3p" rads="..*ay"/>
    <form target="-era" tag="F3s" rads="..*ay"/>
    <form target="-erai" tag="F1s" rads="..*ay"/>
    <form target="-eraient" tag="C3p" rads="..*ay"/>
    <form target="-erais" tag="C12s" rads="..*ay"/>
    <form target="-erait" tag="C3s" rads="..*ay"/>
    <form target="-eras" tag="F2s" rads="..*ay"/>
    <form target="-erez" tag="F2p" rads="..*ay"/>
    <form target="-eriez" tag="C2p" rads="..*ay"/>
    <form target="-erions" tag="C1p" rads="..*ay"/>
    <form target="-erons" tag="F1p" rads="..*ay"/>
    <form target="-eront" tag="F3p" rads="..*ay"/>
    <form target="-es" tag="PS2s" rads="..*ay"/>
    <form target="<" tag="Kms" rads="courbatur"/>
    <form target="<e" tag="Kfs" rads="courbatur"/>
    <form target="<es" tag="Kfp" rads="courbatur"/>
    <form target="<s" tag="Kmp" rads="courbatur"/>
    <derivation target="age" table="nc-2"/>
    <derivation target="e" table="nc-2"/>
    <derivation target="ement" table="nc-2"/>
    <derivation target="ation" table="nc-2"/>
    <derivation target="cation" table="nc-2"/>
    <derivation target="at" table="nc-teur"/>
    <derivation target="cat" table="nc-teur"/>
    <derivation target="ure" table="nc-2"/>
    <derivation target="oir" table="nc-4"/>
    <derivation target="et" table="nc-et"/>
    <derivation target="ette" table="nc-2"/>
    <derivation target="isme" table="nc-2"/>
    <derivation target="iste" table="nc-2"/>
    <derivation target="eu" table="nc-eur"/>
</table>
<table name="v-fiche" source="e" rads=".*fich">
	<like name="v-er"/>
    <form target="e" tag="W" show="#"/>
</table>
<!-- 2EME GROUPE -->
<table name="v-ir2" source="r" rads="..*i">
    <form target="r" tag="W" show="#"/>
    <form target="s" tag="Kmp"/>
    <form target="" tag="Kms"/>
    <form target="e" tag="Kfs"/>
    <form target="es" tag="Kfp"/>
    <form target="ra" tag="F3s"/>
    <form target="rai" tag="F1s"/>
    <form target="raient" tag="C3p"/>
    <form target="rais" tag="C12s"/>
    <form target="rait" tag="C3s"/>
    <form target="ras" tag="F2s"/>
    <form target="rent" tag="J3p"/>
    <form target="rez" tag="F2p"/>
    <form target="riez" tag="C2p"/>
    <form target="rions" tag="C1p"/>
    <form target="rons" tag="F1p"/>
    <form target="ront" tag="F3p"/>
    <form target="s" tag="PJ12s"/>
    <form target="s" tag="Y2s"/>
    <form target="ssaient" tag="I3p"/>
    <form target="ssais" tag="I12s"/>
    <form target="ssait" tag="I3s"/>
    <form target="ssant" tag="G"/>
    <form target="sse" tag="S13s"/>
    <form target="sse" tag="T1s"/>
    <form target="ssent" tag="PS3p"/>
    <form target="ssent" tag="T3p"/>
    <form target="sses" tag="ST2s"/>
    <form target="ssiez" tag="I2p"/>
<!--    <form target="ssiez" tag="ST2p"/>-->
	<form target="ssiez" tag="S2p"/>
	<form target="ssiez" tag="T2p"/>
	
    <form target="ssions" tag="I1p"/>

<!--    <form target="ssions" tag="ST1p"/>-->
    <form target="ssions" tag="S1p"/>
    <form target="ssions" tag="T1p"/>
	
    <form target="ssons" tag="Y1p"/>
    <form target="ssons" tag="P1p" show="#"/>
    <form target="ssez" tag="Y2p"/>
    <form target="ssez" tag="P2p"/>
<!--    <form target="t" tag="PJ3s" show="#"/>-->
    <form target="t" tag="P3s" show="#"/>
    <form target="t" tag="J3s" show="#"/>
	
    <form target="^mes" tag="J1p"/>
    <form target="^t" tag="T3s"/>
    <form target="^tes" tag="J2p"/>
    <derivation target="ssage" table="nc-2"/>
    <derivation target="sseu" table="nc-eur"/>
    <derivation target="ssement" table="nc-2"/>
</table>
<table name="v-haïr" source="r" rads="<haï">
	<like name="v-ir2" except="PJ12s"/>
    <form target="=t" tag="P3s" show="#"/>
    <form target="=s" tag="P12s"/>
    <form target="=s" tag="Y2s"/>
    <form target="s" tag="J12s"/>
</table>
<table name="v-maudire" source="re" rads="<maudi">
	<like name="v-ir2"/>
    <form target="re" tag="W" show="#"/>
    <form target="ts" tag="Kmp"/>
    <form target="t" tag="Kms" show="(#)"/>
    <form target="te" tag="Kfs"/>
    <form target="tes" tag="Kfp"/>
</table>
<!-- 3EME GROUPE -->
<table name="v-aller" source="" rads="(r|)all">
	<like name="v-er" except="PS13s PS3p PS2s"/>	
    <form target="<aille" tag="S13s"/>
    <form target="<aillent" tag="S3p"/>
    <form target="<ailles" tag="S2s"/>
    <form target="<ira" tag="F3s" show="#"/>
    <form target="<irai" tag="F1s"/>
    <form target="<iraient" tag="C3p"/>
    <form target="<irais" tag="C12s"/>
    <form target="<irait" tag="C3s"/>
    <form target="<iras" tag="F2s"/>
    <form target="<irez" tag="F2p"/>
    <form target="<iriez" tag="C2p"/>
    <form target="<irions" tag="C1p"/>
    <form target="<irons" tag="F1p"/>
    <form target="<iront" tag="F3p"/>
    <form target="<va" tag="P3s" show="#"/>
    <form target="<va" tag="Y2s"/>
    <form target="<vais" tag="P1s"/>
    <form target="<vas" tag="P2s"/>
    <form target="<vont" tag="P3p"/>
</table>
<table name="v-ir3" source="" rads=".*(bouill|dorm|serv|..t|ten|ven|enfui|fui|oï|.*quér|assey|rassey|sey|messey|end|batt|romp|.*eiŋ|.*oiŋ|.*aiŋ|.*vainqu)">
    <form target="r" tag="W" rads=".*(end|batt|romp|.*eiŋ|.*oiŋ|.*aiŋ|.*vainqu|pren|mett|assey|rassey|sey|messey)" show="#"/>
    <form target="ir" tag="W" rads=".*(bouill|dorm|serv|.\Tt|ten|ven|enfui|fui|oï|.*quér|.*ill|.*r)" show="#"/>
    <form target="-s" tag="P12s"/>
    <form target="-s" tag="Y2s"/>
    <form target="-t" tag="P3s" show="#"/>
    <form target="aient" tag="I3p"/>
    <form target="ais" tag="I12s"/>
    <form target="ait" tag="I3s"/>
    <form target="ant" tag="G"/>
    <form target="-e" tag="S13s"/>
    <form target="-ent" tag="PS3p"/>
    <form target="-es" tag="S2s"/>
    <form target="ez" tag="Y2p"/>
    <form target="ez" tag="P2p"/>
    <form target="iez" tag="I2p"/>
    <form target="iez" tag="S2p"/>
    <form target="ions" tag="I1p"/>
    <form target="ions" tag="S1p"/>
    <form target="-ra" tag="F3s"/>
    <form target="-rai" tag="F1s"/>
    <form target="-raient" tag="C3p"/>
    <form target="-rais" tag="C12s"/>
    <form target="-rait" tag="C3s"/>
    <form target="-ras" tag="F2s"/>
    <form target="irent" tag="J3p"/>
    <form target="-rez" tag="F2p"/>
    <form target="-riez" tag="C2p"/>
    <form target="-rions" tag="C1p"/>
    <form target="-rons" tag="F1p"/>
    <form target="-ront" tag="F3p"/>
    <form target="isse" tag="T1s"/>
    <form target="isses" tag="T2s"/>
    <form target="issent" tag="T3p"/>
	<form target="îmes" tag="J1p"/>
    <form target="ît" tag="T3s"/>
    <form target="îtes" tag="J2p"/>
    <form target="issiez" tag="T2p"/>
    <form target="issions" tag="T1p"/>
    <form target="is" tag="J12s"/>
    <form target="it" tag="J3s"/>
    <form target="ons" tag="Y1p"/>
    <form target="ons" tag="P1p" show="#"/>
    <form target="is" tag="Kmp" rads="..*(\N|\Tt)"/>
    <form target="i" tag="Kms" rads="..*(\N|\Tt)"/>
    <form target="ie" tag="Kfs" rads="..*(\N|\Tt)"/>
    <form target="ies" tag="Kfp" rads="..*(\N|\Tt)"/>
    <form target="us" tag="Kmp" rads="..*(en|end|omp|tt|qu)"/>
    <form target="u" tag="Kms" rads="..*(en|end|omp|tt|qu)"/>
    <form target="ue" tag="Kfs" rads="..*(en|end|omp|tt|qu)"/>
    <form target="ues" tag="Kfp" rads="..*(en|end|omp|tt|qu)"/>
    <form target="ts" tag="Kmp" rads="...*ŋ"/>
    <form target="t" tag="Kms" rads="...*ŋ"/>
    <form target="te" tag="Kfs" rads="...*ŋ"/>
    <form target="tes" tag="Kfp" rads="...*ŋ"/>
    <form target="s" tag="Kms" rads=".*(assey|rassey|sey|messey|.*quér)"/>
    <form target="s" tag="Kmp" rads=".*(assey|rassey|sey|messey|.*quér)"/>
    <form target="se" tag="Kfs" rads=".*(assey|rassey|sey|messey|.*quér)"/>
    <form target="ses" tag="Kfp" rads=".*(assey|rassey|sey|messey|.*quér)"/>
</table>
<table name="v-faillir" source="" rads="faill">
	<like name="v-ir3"/>
    <form target="e" tag="Y2s" show="#"/>
</table>
<table name="v-vêtir" source="" rads=".*vêt">
	<like name="v-ir3"/>
    <form target="-ts" tag="P12s"/>
    <form target="-ts" tag="Y2s"/>
    <form target="is" tag="P12s"/>
    <form target="-t" tag="P3s" show="#"/>
    <form target="it" tag="P3s" show="(#)"/>
    <form target="us" tag="Kmp"/>
    <form target="u" tag="Kms" show="#"/>
    <form target="ue" tag="Kfs"/>
    <form target="ues" tag="Kfp"/>
</table>
<table name="v-prendre" source="" rads=".*(pren|mett)">
	<like name="v-ir3" except="Kms Kmp"/>
    <form target="ds" tag="P12s"/>
    <form target="ds" tag="Y2s"/>
    <form target="d" tag="P3s"/>
    <form target="<is" tag="Km"/>
    <form target="<ise" tag="Kfs"/>
    <form target="<ises" tag="Kfp"/>
    <form target="<is" tag="J12s"/>
    <form target="<it" tag="J3s"/>
    <form target="<îmes" tag="J1p"/>
    <form target="<îtes" tag="J2p"/>
    <form target="<irent" tag="J3p"/>
    <form target="<isse" tag="T1s"/>
    <form target="<ît" tag="T3s"/>
    <form target="<isses" tag="T2s"/>
    <form target="<issions" tag="T1p"/>
    <form target="<issiez" tag="T2p"/>
    <form target="<issent" tag="T3p"/>
</table>
<table name="v-assaillir" source="" rads="(assaill|tressaill|défaill|saill|accueill|recueill|cueill)">
	<like name="v-ir3" except="P12s P3s S13s S2s"/>
    <form target="e" tag="PS13s"/>
    <form target="e" tag="Y2s" show="#"/>
    <form target="es" tag="PS2s"/>
</table>
<table name="v-courir" source="" rads=".*cour">
	<like name="v-assaillir" except="PS13s PS2s T1s T3s"/>
    <form target="-s" tag="P12s"/>
    <form target="-s" tag="Y2s"/>
    <form target="-t" tag="P3s"/>
    <form target="us" tag="J12s"/>
    <form target="ut" tag="J3s"/>
    <form target="ûmes" tag="J1p"/>
    <form target="ûtes" tag="J2p"/>
    <form target="urent" tag="J3p"/>
    <form target="usse" tag="T1s"/>
    <form target="ût" tag="T3s"/>
    <form target="usses" tag="T2s"/>
    <form target="ussions" tag="T1p"/>
    <form target="ussiez" tag="T2p"/>
    <form target="ussent" tag="T3p"/>
    <form target="u" tag="Kms"/>
    <form target="ue" tag="Kfs"/>
    <form target="us" tag="Kmp"/>
    <form target="ues" tag="Kfp"/>
</table>
<table name="v-ouvrir" source="" rads=".*(couvr|ouvr|souffr|offr)">
	<like name="v-assaillir"/>
    <form target="e<ts" tag="Kmp"/>
    <form target="e<t" tag="Kms" show="#"/>
    <form target="e<te" tag="Kfs"/>
    <form target="e<tes" tag="Kfp"/>
</table>
<table name="v-mourir" source="" rads="mour">
	<like name="v-courir"/>
    <form target="<ort" tag="Kms"/>
    <form target="<orte" tag="Kfs"/>
    <form target="<orts" tag="Kmp"/>
    <form target="<ortes" tag="Kfp"/>
</table>
<!--<table name="v-ouïr" source="" rads="">-->
<!--    <form target="ouïr" tag="W" show="#"/>-->
<!--    <form target="ouis" tag="Y2s"/>-->
<!--    <form target="oyons" tag="Y1p"/>-->
<!--    <form target="oyez" tag="Y2p"/>-->
<!--</table>-->
<table name="v-gésir" source="" rads="">
    <form target="gésir" tag="W" show="#"/>
    <form target="gis" tag="P12s"/>
    <form target="gît" tag="P3s"/>
    <form target="gisons" tag="P12s"/>
    <form target="gisez" tag="P12s"/>
    <form target="gisent" tag="P12s"/>
    <form target="gisais" tag="I12s"/>
    <form target="gisait" tag="I3s"/>
    <form target="gisions" tag="I12s"/>
    <form target="gisiez" tag="I12s"/>
    <form target="gisaient" tag="I12s"/>
    <form target="gisant" tag="G"/>
</table>
<table name="v-evoir" source="" rads=".*[cd]">
    <form target="evaient" tag="I3p"/>
    <form target="evais" tag="I12s"/>
    <form target="evait" tag="I3s"/>
    <form target="evant" tag="G"/>
    <form target="evez" tag="Y2p"/>
    <form target="evez" tag="P2p"/>
    <form target="eviez" tag="I2p"/>
    <form target="eviez" tag="S2p"/>
    <form target="evions" tag="I1p"/>
    <form target="evions" tag="S1p"/>
    <form target="evoir" tag="W"/>
    <form target="evons" tag="Y1p"/>
    <form target="evons" tag="P1p"/>
    <form target="evra" tag="F3s"/>
    <form target="evrai" tag="F1s"/>
    <form target="evraient" tag="C3p"/>
    <form target="evrais" tag="C12s"/>
    <form target="evrait" tag="C3s"/>
    <form target="evras" tag="F2s"/>
    <form target="evrez" tag="F2p"/>
    <form target="evriez" tag="C2p"/>
    <form target="evrions" tag="C1p"/>
    <form target="evrons" tag="F1p"/>
    <form target="evront" tag="F3p"/>
    <form target="ois" tag="P12s"/>
    <form target="ois" tag="Y2s"/>
    <form target="oit" tag="P3s"/>
    <form target="oive" tag="S13s"/>
    <form target="oivent" tag="PS3p"/>
    <form target="oives" tag="S2s"/>
    <form target="urent" tag="J3p"/>
    <form target="us" tag="J12s"/>
    <form target="usse" tag="T1s"/>
    <form target="ussent" tag="T3p"/>
    <form target="usses" tag="T2s"/>
    <form target="ussiez" tag="T2p"/>
    <form target="ussions" tag="T1p"/>
    <form target="ut" tag="J3s"/>
	<form target="û" tag="Kms" rads=".*d"/>
	<form target="u" tag="Kms" rads=".*c"/>
    <form target="ue" tag="Kfs"/>
    <form target="ues" tag="Kfp"/>
    <form target="ûmes" tag="J1p"/>
	<form target="ûs" tag="Kmp" rads=".*d"/>
	<form target="us" tag="Kmp" rads=".*c"/>
    <form target="ût" tag="T3s"/>
    <form target="ûtes" tag="J2p"/>
</table>
<table name="v-voir" source="" rads="(entre|re|)v">
    <form target="erra" tag="F3s"/>
    <form target="errai" tag="F1s"/>
    <form target="erraient" tag="C3p"/>
    <form target="errais" tag="C12s"/>
    <form target="errait" tag="C3s"/>
    <form target="erras" tag="F2s"/>
    <form target="errez" tag="F2p"/>
    <form target="erriez" tag="C2p"/>
    <form target="errions" tag="C1p"/>
    <form target="errons" tag="F1p"/>
    <form target="erront" tag="F3p"/>
    <form target="irent" tag="J3p"/>
    <form target="is" tag="J12s"/>
    <form target="isse" tag="T1s"/>
    <form target="issent" tag="T3p"/>
    <form target="isses" tag="T2s"/>
    <form target="issiez" tag="T2p"/>
    <form target="issions" tag="T1p"/>
    <form target="it" tag="J3s"/>
    <form target="oie" tag="S13s"/>
    <form target="oient" tag="PS3p"/>
    <form target="oies" tag="S2s"/>
    <form target="oir" tag="W"/>
    <form target="ois" tag="P12s"/>
    <form target="ois" tag="Y2s"/>
    <form target="oit" tag="P3s"/>
    <form target="oyaient" tag="I3p"/>
    <form target="oyais" tag="I12s"/>
    <form target="oyait" tag="I3s"/>
    <form target="oyant" tag="G"/>
    <form target="oyez" tag="Y2p"/>
    <form target="oyez" tag="P2p"/>
    <form target="oyiez" tag="I2p"/>
    <form target="oyiez" tag="S2p"/>
    <form target="oyions" tag="I1p"/>
    <form target="oyions" tag="S1p"/>
    <form target="oyons" tag="Y1p"/>
    <form target="oyons" tag="P1p"/>
    <form target="u" tag="Kms"/>
    <form target="ue" tag="Kfs"/>
    <form target="ues" tag="Kfp"/>
    <form target="us" tag="Kmp"/>
    <form target="îmes" tag="J1p"/>
    <form target="ît" tag="T3s"/>
    <form target="îtes" tag="J2p"/>
</table>
<table name="v-prévoir" source="" rads="(re|)prév">
	<like name="v-voir"/>
    <form target="oira" tag="F3s"/>
    <form target="oirai" tag="F1s"/>
    <form target="oiraient" tag="C3p"/>
    <form target="oirais" tag="C12s"/>
    <form target="oirait" tag="C3s"/>
    <form target="oiras" tag="F2s"/>
    <form target="oirez" tag="F2p"/>
    <form target="oiriez" tag="C2p"/>
    <form target="oirions" tag="C1p"/>
    <form target="oirons" tag="F1p"/>
    <form target="oiront" tag="F3p"/>
</table>
<table name="v-pourvoir" source="" rads="(re|)(dé|)pourv">
	<like name="v-prévoir"/>
    <form target="urent" tag="J3p"/>
    <form target="us" tag="J12s"/>
    <form target="usse" tag="T1s"/>
    <form target="ussent" tag="T3p"/>
    <form target="usses" tag="T2s"/>
    <form target="ussiez" tag="T2p"/>
    <form target="ussions" tag="T1p"/>
    <form target="ut" tag="J3s"/>
    <form target="ûmes" tag="J1p"/>
    <form target="ût" tag="T3s"/>
    <form target="ûtes" tag="J2p"/>
</table>
<table name="v-savoir" source="" rads="s">
	<!-- très similaire à v-evoir et à avoir : à creuser -->
    <form target="avaient" tag="I3p"/>
    <form target="avais" tag="I12s"/>
    <form target="avait" tag="I3s"/>
    <form target="achant" tag="G"/>
    <form target="achez" tag="Y2p"/>
    <form target="avez" tag="P2p"/>
    <form target="aviez" tag="I2p"/>
    <form target="achiez" tag="S2p"/>
    <form target="avions" tag="I1p"/>
    <form target="achions" tag="S1p"/>
    <form target="avoir" tag="W"/>
    <form target="achons" tag="Y1p"/>
    <form target="avons" tag="P1p"/>
    <form target="aura" tag="F3s"/>
    <form target="aurai" tag="F1s"/>
    <form target="auraient" tag="C3p"/>
    <form target="aurais" tag="C12s"/>
    <form target="aurait" tag="C3s"/>
    <form target="auras" tag="F2s"/>
    <form target="aurez" tag="F2p"/>
    <form target="auriez" tag="C2p"/>
    <form target="aurions" tag="C1p"/>
    <form target="aurons" tag="F1p"/>
    <form target="auront" tag="F3p"/>
    <form target="ais" tag="P12s"/>
    <form target="ache" tag="Y2s"/>
    <form target="ait" tag="P3s"/>
    <form target="ache" tag="S13s"/>
    <form target="avent" tag="P3p"/>
    <form target="achent" tag="S3p"/>
    <form target="aches" tag="S2s"/>
    <form target="urent" tag="J3p"/>
    <form target="us" tag="J12s"/>
    <form target="usse" tag="T1s"/>
    <form target="ussent" tag="T3p"/>
    <form target="usses" tag="T2s"/>
    <form target="ussiez" tag="T2p"/>
    <form target="ussions" tag="T1p"/>
    <form target="ut" tag="J3s"/>
	<form target="u" tag="Kms"/>
    <form target="us" tag="Kmp"/>
    <form target="ue" tag="Kfs"/>
    <form target="ues" tag="Kfp"/>
    <form target="ûmes" tag="J1p"/>
    <form target="ût" tag="T3s"/>
    <form target="ûtes" tag="J2p"/>
</table>
<table name="v-seoir" source="" rads="(ass|rass)oi">
	<like name="v-prendre"/>
    <form target="e<r" tag="W"/>
    <form target="s" tag="P12s"/>
    <form target="s" tag="Y2s"/>
    <form target="t" tag="P3s"/>
</table>
<table name="v-surseoir" source="" rads="sursoi">
	<like name="v-seoir"/>
    <form target="e<ra" tag="F3s"/>
    <form target="e<rai" tag="F1s"/>
    <form target="e<raient" tag="C3p"/>
    <form target="e<rais" tag="C12s"/>
    <form target="e<rait" tag="C3s"/>
    <form target="e<ras" tag="F2s"/>
    <form target="e<rez" tag="F2p"/>
    <form target="e<riez" tag="C2p"/>
    <form target="e<rions" tag="C1p"/>
    <form target="e<rons" tag="F1p"/>
    <form target="e<ront" tag="F3p"/>
</table>
<table name="v-choir" source="" rads="(ch|rech|éch)oi">
	<like name="v-ir3" except="I Y"/>
    <form target="<u" tag="Kms"/>
    <form target="<us" tag="Kmp"/>
    <form target="<use" tag="Kfs"/>
    <form target="<ues" tag="Kfp"/>
    <form target="<urent" tag="J3p"/>
    <form target="<us" tag="J12s"/>
    <form target="<usse" tag="T1s"/>
    <form target="<ussent" tag="T3p"/>
    <form target="<usses" tag="T2s"/>
    <form target="<ussiez" tag="T2p"/>
    <form target="<ussions" tag="T1p"/>
    <form target="<ut" tag="J3s"/>
    <form target="<ûmes" tag="J1p"/>
    <form target="<ût" tag="T3s"/>
    <form target="<ûtes" tag="J2p"/>
</table>
