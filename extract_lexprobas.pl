#!/usr/bin/env perl

use strict;

my %temp_proba=();
my %proba_lemme_faux=();
my %proba_tag_sachant_forme_TF=();
my $forme;
my $tag;
my $cat;
my $proba;
while (<STDIN>) {
    chomp;
    s/^([^\t]*)\t// || die("Bad format in input file: $_\n");
    $forme=$1;
    %temp_proba=();
    for (split(/\|/,$_)) {
	/^([^\t]*)\t([^\t]*)$/ || die ("Bad format in input file: ltp $_\n");
	$tag=$1;
	$proba=$2;
	if ($proba>0) {
	    $proba_tag_sachant_forme_TF{$tag}{$forme}+=$proba;
	    $proba_tag_sachant_forme_TF{__ALL__}{$forme}+=$proba;
	}
    }
}
for $tag (keys %proba_tag_sachant_forme_TF) {
    if ($tag ne "__ALL__") {
	for $forme (keys %{$proba_tag_sachant_forme_TF{$tag}}) {
	    $proba_tag_sachant_forme_TF{$tag}{$forme}/=$proba_tag_sachant_forme_TF{__ALL__}{$forme};
	}
    }
}
delete($proba_tag_sachant_forme_TF{__ALL__});

for $tag (keys %proba_tag_sachant_forme_TF) {
    for $forme (keys %{$proba_tag_sachant_forme_TF{$tag}}) {
	print "$tag\t$forme\t$proba_tag_sachant_forme_TF{$tag}{$forme}\n";
    }
}

# my $okfilename=shift;
# open(OK,"< $okfilename") || die("$okfilename:$!\n");
# while (<OK>) {
#     chomp;
#     /^([^\t]*)\t[^\t]*\t[^\t]*\t([^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t[^\t]*\t[^\t]*\t([^\t]*))$/ || die("Bad format in $okfilename: $_\n");
#     if (defined($proba_lemme_faux{$3."\t".$4})) {
# 	print "$1\t\t".(1-$proba_lemme_faux{$3."\t".$4})."\t$2\n";
#     } else {
# 	print $_."\n";
#     }
# }
# close(OK);

