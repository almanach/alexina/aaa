# emploi typique:
# perl ranker.pl sk.ok sk.occcomp sk.nlf sk.ntlf sk.validatedlexicon sk.prefs sk.lexprobas sk.der

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $stepmax=15;
my $probainitiale=0.05;
my $proba_lemme_eliminatoire=0.00000001;
my $loglevel=2;

my $okfile=shift;
my $occcompfile=shift;
my $nlffile=shift;
my $ntlffile=shift;
my $validatedlexiconfile=shift;
my $prefsfile=shift;
my $lexprobasfile=shift;
my $derfile=shift;

print STDERR "  LEXICON EXTRACTOR --- okfile=$okfile occcompfile=$occcompfile nlffile=$nlffile ntlffile=$ntlffile validatedlexiconfile=$validatedlexiconfile prefsfile=$prefsfile lexprobasfile=$lexprobasfile\n";

my $corpus_tagge=0;

my $arg=$okfile;
$arg=~s/^(.*)\.[^\.]*$/$1/; 
open(TRACE, "> $arg.trace") || die("$!\n"); #ouverture du fichier en lecture  
open(LOG, "> $arg.log") || die("$!\n"); #ouverture du fichier en lecture  
open(TAGDATA, "> $arg.tagdata") || die("$!\n"); #ouverture du fichier en lecture  

# opening files first: it's better to know now that one file is missing than after having spend time to parse some of them
open(PREFSFILE, $prefsfile) || die("$prefsfile not found $!\n"); #ouverture du fichier en lecture  
open(OCCCOMPFILE, $occcompfile) || die("$occcompfile not found $!\n"); #ouverture du fichier en lecture  
open(NLFFILE, $nlffile) || die("$nlffile not found $!\n"); #ouverture du fichier en lecture  
open(NTLFFILE, $ntlffile) || die("$ntlffile not found $!\n"); #ouverture du fichier en lecture  
open(VLEXFILE, $validatedlexiconfile) || die("$validatedlexiconfile not found $!\n"); #ouverture du fichier en lecture  
open(LPFILE, $lexprobasfile) || die("$lexprobasfile not found $!\n"); #ouverture du fichier en lecture  
open(DERFILE, $derfile) || die("$derfile not found $!\n"); #ouverture du fichier en lecture  
open(OKFILE, $okfile) || die("$arg not found $!\n"); #ouverture du fichier en lecture  


# charger les préfixes
my %probapref=();
my %repref=();
my $pref="";
print STDERR "    Loading prefixes and related information...";
while (<PREFSFILE>) { 
    chomp; 
    /^([^\t]*)\t([^\t]*)\t([0-9\.]*)$/ || die("Bad format in file $prefsfile: $_\n");
    $pref=$1;
    $probapref{$pref}=$3;
    $repref{$pref}=qr/^$pref($2.*)$/;
}
close(PREFSFILE);
print STDERR "done\n";

print STDERR "    Loading forms and their occurrences...";
# charger les formes et occurences d'icelles
my %occurrences=();
my %occurrencestotales=();
my $nmots=0;
my %proba_forme=();
while (<OCCCOMPFILE>) { 
    chomp; 
    /^([^\t]*)\t([^\t]*)\t(.*)$/;
    $occurrences{$1}=$3;
    $occurrencestotales{$1}=max($3,$2);
    $proba_forme{$1}=$3;
    $nmots+=$3;
}
for (keys %proba_forme) {
    $proba_forme{$_}/=$nmots;
}
close(OCCCOMPFILE);
print STDERR "done\n";

print STDERR "    Loading the number of hypothetical lemmas generating each form...";
# charger le nombre de lemme pouvant generer chaque forme (fichier *.nlf)
my %nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT=();
while (<NLFFILE>) { 
     chomp; 
     /^([^\t]*\t[^\t]*\t[^\t]*)\t([^\t]*)$/ || die("Bad format in file $nlffile: $_\n");
     $nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT{$1}=$2;
}
close(NLFFILE);
print STDERR "done\n";

print STDERR "    Loading the number of different tags for each possible form-lemma pair...";
# (fichier *.ntlf)
my %nombre_tags_possibles_sachant_forme_et_lemme_FL=();
while (<NTLFFILE>) { 
     chomp; 
     /^([0-9]*)\s+([^\t]*)\t([^\t]*)\t([^\t]*\t[^\t]*)$/ || die("Bad format in file $ntlffile: $_\n");
     $nombre_tags_possibles_sachant_forme_et_lemme_FL{$2.$3}{$4}=$1;
}
close(NTLFFILE);
print STDERR "done\n";

print STDERR "    Loading previously validated lemmas (current lexicon)...";
# fichier *.validatedlexicon
my %lemmes_connus=();
while (<VLEXFILE>) { 
     chomp; 
     if (/^([^\t]*\t[^\t]+)$/) {
	 $lemmes_connus{$1}=1;
     }
}
close(VLEXFILE);
print STDERR "done\n";

print STDERR "    Loading quantitative data if the corpus has been already tagged...";
my %proba_cat_et_tag_sachant_forme_CatTF=();
my $proba; my $cat; my $tag=""; my $catettag; my $forme;
while (<LPFILE>) { 
     chomp; 
     /^([^\t]*)\t([^\t]*)\t([^\t]*)$/ || die("Bad format in file $lexprobasfile: $_");
     $corpus_tagge=1;
     $catettag=$1;
     $forme=$2;
     $proba=$3;
     if ($catettag=~/^([^-]*)-(.*)$/) {$cat=$1; $tag=$2} else {$cat=$catettag; $tag=""}
     $proba_cat_et_tag_sachant_forme_CatTF{$cat}{$tag}{$forme}=$proba;
}
close(LPFILE);
print STDERR "done\n";

# charge les d�rivations
my %lemme_derive_de_lemme_LLd=();
my %lemme_origine_de_lemme_LdL=();
print STDERR "    Loading derivational morphology information...";
my $der;
my $lemme;
while (<DERFILE>) { 
    chomp; 
    /^([^\t]*\t[^\t]*)\t([^\t]*\t[^\t]*)$/ || die("Bad format in file $derfile: $_\n");
    $lemme=$1;
    $der=$2;
    $lemme_derive_de_lemme_LLd{$lemme}{$der}=1;
    $lemme_origine_de_lemme_LdL{$der}{$lemme}=1;
}
my $donesomething=1;
while ($donesomething) {
    print STDERR ".";
    $donesomething=0;
    for my $lemmeo (keys %lemme_derive_de_lemme_LLd) {
	for my $lemmed (keys %{$lemme_derive_de_lemme_LLd{$lemmeo}}) {
	    for my $lemmed2 (keys %{$lemme_derive_de_lemme_LLd{$lemmed}}) {
		if (!defined($lemme_derive_de_lemme_LLd{$lemmeo}{$lemmed2})) {
		    $lemme_derive_de_lemme_LLd{$lemmeo}{$lemmed2}=$lemme_derive_de_lemme_LLd{$lemmed}{$lemmed2}+1;
		    $lemme_origine_de_lemme_LdL{$lemmed2}{$lemmeo}=$lemme_derive_de_lemme_LLd{$lemmeo}{$lemmed2};
		    $donesomething=1;
		}
	    }
	}
    }
}
close(DERFILE);
print STDERR "done\n";



# charger le formes et occurences d'icelles
my %lemmes=(); # proba que le lemme soit valide
my %decoupages=();
my %conj_de_lemme=();
my %gconj_de_lemme=();
my %cat_de_lemme=();
my %occ=(); # occ des lemmes
my $nonp=0;
my $l=""; my $f=""; my $p=0;
my $rad=""; my $conj;
my $count=0;
my %lemmes_generant_forme_FL=();
my %formes_issues_de_lemme_LF=();
my %formes_absentes_issues_de_lemme_LF=();
my %lemme_derive_de_forme_de_lemme_FL=();
my $proba_lemmetag_faux_sachant_forme;
my %nombre_de_formetags_de_lemme=();
print STDERR "    Loading lemmas and their forms...";
my $nltotal;
my $n=0;
while (<OKFILE>) { 
    if ($n % 10000 == 0) {print STDERR "\r    Loading lemmas and their forms... $n";}
    $n++;
    chomp;
    /^[^\t]*\t[^\t]*\t([^\t]*)\t([^\t]*)\t([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)\t([^\t]*)$/ || die("Bad format in input file: $_\n");
    $p=$1;
    $l=$4."\t".$6;
    $rad=$2;
    $f=$2.$3;
    $tag=$5;
    $conj_de_lemme{$l}=$6;
    $conj_de_lemme{$l}=~/^([^-]+)/ || die "Pas de cat reconnue dans $conj_de_lemme{$l} (lemme $l)\n";
    $cat_de_lemme{$l}=$1;
    $conj_de_lemme{$l}=~/^([^:]+)/;
    $gconj_de_lemme{$l}=$1;
    $nombre_de_formetags_de_lemme{$l}++;
    if (!defined($lemmes{$l})) {
    }
    if (defined($lemmes_connus{$l})) {
	$lemmes{$l}+=1;
	$proba_lemmetag_faux_sachant_forme=0;
    } elsif (!defined($proba_cat_et_tag_sachant_forme_CatTF{$cat_de_lemme{$l}}{$tag}{$f})) {
	if ($corpus_tagge) {
	    $proba_lemmetag_faux_sachant_forme=1;
	} else {
	    if ($nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT{$f."\t".$cat_de_lemme{$l}."\t".$tag} == 0) {
		die("Bug: \$nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT{$f\t$cat_de_lemme{$l}\t$tag}=0 but it corresponds to an entry in the .ok file\n");
	    }
	    if ($occurrences{$f}>0) {
		$proba_lemmetag_faux_sachant_forme=(1-1/$nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT{$f."\t__ALL__\t__ALL__"});
		$lemmes{$l}+=(1-$proba_lemmetag_faux_sachant_forme);
		&log_print("$f\t\$lemmes{$l}+=(1-$proba_lemmetag_faux_sachant_forme)=".(1-$proba_lemmetag_faux_sachant_forme)." -> $lemmes{$l}\n",2);
	    }
	}
    } else {
	if ($occurrences{$f}>0) {
	    $p=$proba_cat_et_tag_sachant_forme_CatTF{$cat_de_lemme{$l}}{$tag}{$f};
	    if ($p > 0) {
		$proba_lemmetag_faux_sachant_forme=$p*(1-1/$nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT{$f."\t".$cat_de_lemme{$l}."\t".$tag})+(1-$p);
		$lemmes{$l}+=(1-$proba_lemmetag_faux_sachant_forme);
		&log_print("$f\t\$lemmes{$l}+=(1-$proba_lemmetag_faux_sachant_forme) -> $lemmes{$l}\t(LEXPROBA)\n",2);
	    } else {
		$proba_lemmetag_faux_sachant_forme=1;
	    }
	}
    }
    if ($occurrences{$f}>0 && $nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT{$f."\t__ALL__\t__ALL__"}>0) {
	&log_print("($f)\t$l\tocc+=".(1-$proba_lemmetag_faux_sachant_forme)."*".$occurrences{$f}."=".((1-$proba_lemmetag_faux_sachant_forme)*$occurrences{$f}),2);
	$occ{$l}+=(1-$proba_lemmetag_faux_sachant_forme)*$occurrences{$f};
	&log_print(" ---> occ{$l}=$occ{$l}\n",2);
	$lemmes_generant_forme_FL{$f}{$l}=1;
	$formes_issues_de_lemme_LF{$l}{$f}=1;
	if ($tag=~/^D\[_(.*)\]/) {
	    push(@{$lemme_derive_de_forme_de_lemme_FL{$f}{$l}},$rad.$1);
	}
    } elsif ($tag!~/^D\[_(.*)\]/) { # les dérivés absents ne pénalisent pas un lemme: on ne prend pas en compte l'info de leur absence
	$formes_absentes_issues_de_lemme_LF{$l}{$f}=1;
    }
}
my $occtotal;
for $l (keys %occ) {
    $occtotal+=$occ{$l};
}
for $l (keys %occ) {
    $occ{$l}*=$nmots;
    $occ{$l}/=$occtotal;
    &log_print("occ initial de $l : occ_0{$l}=$occ{$l}\n",2);
}
for $l (keys %lemmes) {
    &log_print("proba initiale que le lemme $l soit bon : lemmes_0{$l}=$lemmes{$l}/$nombre_de_formetags_de_lemme{$l} ",2);
    $lemmes{$l}/=$nombre_de_formetags_de_lemme{$l};
    &log_print("= $lemmes{$l}\n",2);
}
%nombre_de_formetags_de_lemme=0;
close(OKFILE);
print STDERR "\r    Loading lemmas and their forms...done (total: $n forms)         \n";

%nombre_tags_possibles_sachant_forme_et_lemme_FL=();
%nbre_lemmes_possibles_generant_forme_et_cat_et_tag_FCatT=();

# nettoyage des lemmes déjà à proba nulle (car à occ nulle)
print STDERR "    Eliminating lemmas which already have a zero probability...";
my $nlelimines;
for $l (keys %occ) {
    $nltotal++;
    if ($occ{$l} == 0) {
	$nlelimines++;
	delete($occ{$l});
	delete($lemmes{$l});
	delete($conj_de_lemme{$l});
	delete($gconj_de_lemme{$l});
	delete($cat_de_lemme{$l});
	delete($formes_absentes_issues_de_lemme_LF{$l});
	for $f (keys %{$formes_issues_de_lemme_LF{$l}}) {
	    delete($lemmes_generant_forme_FL{$f}{$l});
	    delete($lemme_derive_de_forme_de_lemme_FL{$f}{$l});
	}
	delete($formes_issues_de_lemme_LF{$l});
    }
}
print STDERR "done        ";
if ($nlelimines>0) {print STDERR " ($nlelimines eliminated out of $nltotal)\n"} else {print STDERR " (no lemma eliminated)\n"}

# PREFIXES
print STDERR "    Looking for possible prefixes in lemmas...";
$n=0;
my $gconj;
foreach $l (keys %lemmes) {
    $n++;
    $cat=$cat_de_lemme{$l};
    $conj=$conj_de_lemme{$l};
    $gconj=$gconj_de_lemme{$l};
    if ($n % 10000 == 0) {print STDERR "\r    Looking for possible prefixes in lemmas... $n";}
    foreach $pref (keys %repref) {
	if ($l=~$repref{$pref}) {
	    $rad=$1;
	    if (defined($lemmes{$rad})) {
		&log_print("Je rajoute au lemme $l l'hypothèse d'un lemme à préfixe $pref-$rad\n",1);
		$nltotal++;
		$lemmes{$pref."#".$rad}=$lemmes{$pref.$rad};
		$occ{$pref."#".$rad}=$occ{$l};
		$cat_de_lemme{$pref."#".$rad}=$cat;
		$conj_de_lemme{$pref."#".$rad}=$conj;
		$gconj_de_lemme{$pref."#".$rad}=$gconj;
		$decoupages{$pref.$rad}{$pref."#".$rad}=1;
		for $f (keys %{$formes_issues_de_lemme_LF{$l}}) {
		    $lemmes_generant_forme_FL{$f}{$pref."#".$rad}=1;
		    $formes_issues_de_lemme_LF{$pref."#".$rad}{$f}=1;
		    if (defined $lemme_derive_de_forme_de_lemme_FL{$f}{$l}) {
			@{$lemme_derive_de_forme_de_lemme_FL{$f}{$pref."#".$rad}}=@{$lemme_derive_de_forme_de_lemme_FL{$f}{$l}};
		    }
		}
		for $f (keys %{$formes_absentes_issues_de_lemme_LF{$l}}) {
		    $formes_absentes_issues_de_lemme_LF{$pref."#".$rad}{$f}=1;
		}
	    }
	}
    }
}
print STDERR "\r    Looking for possible prefixes in lemmas... done      \n";

my $rad=""; my $term="";
my $lemmecomplet=""; my $formname="";
my $pos=""; my $ending="";
my $proba=0; my $contrib=0; my $count=0;
my $fl=0; my $pl=""; my $odd=1;
my $denominateur=1;
my $autrelemme="";
my $flproba=-1;
my $step=0;

my %oldlemmes=();

sub max{
    if (@_[0] > @_[1]) {
	return @_[0];
    } else {
	return @_[1];
    }
}

sub min{
    if (@_[0] < @_[1]) {
	return @_[0];
    } else {
	return @_[1];
    }
}

my $c="";
my $nf="";
my $infiniteodd=0;
my $alreadyinfiniteodd=0;
my $contrib=0;
my %oldocc=();
my %proba_tag_sachant_conj=();
my %nombreformesparterm=();
my %termdeform=();
my %proba_tag_sachant_forme_et_lemme_FLT=();

print STDERR "    Initialization..";
open(OKFILE, $okfile) || die("$okfile not found $!\n"); #ouverture du fichier en lecture  
%proba_tag_sachant_conj=();
%nombreformesparterm=();
%termdeform=();
my %proba_forme_sachant_lemme_FL=();
my %proba_term_sachant_conj_TC=();
my %term_de_forme_sachant_lemme_FL=();
my %proba_de_term_sachant_conj_et_tag_CTagT=();
my %proba_lemme=();
my %odds_lemme=();
my %proba_derive_valide_LLd=();
for $lemme (keys %occ) {
    $proba_lemme{$lemme}=$occ{$lemme}/$nmots;
    $odds_lemme{$lemme}=$proba_lemme{$lemme}/(1-$proba_lemme{$lemme});
    &log_print("proba initiale de tomber sur $lemme : proba_lemme_0{$lemme}=$proba_lemme{$lemme}\n",2);
}
$n=0;
while (<OKFILE>) { 
    $n++;
    if ($n % 10000 == 0) {print STDERR "\r    Initialization.. $n  ";}
    chomp($_); 
    /^.*\t([^\t]*)\t[^\t]*\t[^\t]*\t([^\t]*)$/ || die("Erreur - ligne:\n|$_|\n");
    $lemme=$1."\t".$2;
    if (defined($occ{$lemme})) {
	/^[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t([^\t]*)\t[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t[^\t]*$/ || die("Erreur - ligne:\n|$_|\n");
	$forme=$1.$2;
	$term=$2;
	$tag=$3;
	if ($corpus_tagge) {
	    if (defined($proba_cat_et_tag_sachant_forme_CatTF{$cat_de_lemme{$lemme}}{$tag}{$forme})) {
		$p=$proba_cat_et_tag_sachant_forme_CatTF{$cat_de_lemme{$lemme}}{$tag}{$forme};
	    } else {
		$p=0;
	    }
	} else {$p=1}
	if ($p>0) {
	    $proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}=$p;
	    $proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{__ALL__}+=$p;
	}
	$term_de_forme_sachant_lemme_FL{$forme}{$lemme}=$term;
# PREFIXES
	foreach $pref (keys %repref) {
	    if ($lemme=~$repref{$pref}) {
		$rad=$1;
		if ($lemmes{$rad}>0) {
		    $proba_lemme{$pref."#".$rad}=$proba_lemme{$lemme};
		    $odds_lemme{$pref."#".$rad}=$odds_lemme{$lemme};
		    $proba_tag_sachant_forme_et_lemme_FLT{$forme}{$pref."#".$rad}{$tag}=$p;
		    $proba_tag_sachant_forme_et_lemme_FLT{$forme}{$pref."#".$rad}{__ALL__}+=$p;
		    $term_de_forme_sachant_lemme_FL{$forme}{$pref."#".$rad}=$term;
		}
	    }
	}
	if ($occurrences{$forme}*$lemmes{$lemme}>0) {
	    $proba_forme_sachant_lemme_FL{$forme}{$lemme}+=$occurrences{$forme}*$lemmes{$lemme};
	    $proba_forme_sachant_lemme_FL{__ALL__}{$lemme}+=$occurrences{$forme}*$lemmes{$lemme};
# PREFIXES
	    foreach $pref (keys %repref) {
		if ($lemme=~$repref{$pref}) {
		    $rad=$1;
		    if ($lemmes{$rad}>0) {
			$proba_forme_sachant_lemme_FL{$forme}{$pref."#".$rad}=$proba_forme_sachant_lemme_FL{$forme}{$lemme};
			$proba_forme_sachant_lemme_FL{__ALL__}{$pref."#".$rad}=$proba_forme_sachant_lemme_FL{__ALL__}{$lemme};
		    }
		}
	    }
	}
    }
}
close(OKFILE);
print STDERR "\r    Initialization... normalization [n1]";
&normalize_1_2(\%proba_forme_sachant_lemme_FL);
print STDERR "\r    Initialization... normalization [n2]";
&normalize_3_3(\%proba_tag_sachant_forme_et_lemme_FLT);
print STDERR "\r    Initialization... normalization [d] ";
delete($proba_forme_sachant_lemme_FL{__ALL__});
print STDERR "\r    Initialization... normalization [l] ";
for $forme (keys %proba_forme_sachant_lemme_FL) {
    for $lemme (keys %{$proba_forme_sachant_lemme_FL{$forme}}) {
	&log_print("\$proba_forme_sachant_lemme_FL_0{$forme}{$lemme}=$proba_forme_sachant_lemme_FL{$forme}{$lemme}\n",2);
    }
}
print STDERR "\r    Initialization... done               \n";






my %proba_forme_sachant_lemme_FL_old=();
my %proba_lemme_old=();
my %proba_lemme_sachant_forme_LF=();
my %proba_forme_et_lemme_FL=();
my %lemme_certain=();
my %proba_tag_sachant_cat_TC=();
my %proba_term_sachant_conj_et_tag_CTagT=();
my %proba_conj_sachant_cat_CCat=();
my $moderateur;
my $eliminated_lemmas_total=0;
print STDERR "    Step  0          .......\r";
for ($step=1; $step<=$stepmax; $step++) {
    print STDERR "    Step ";
    if ($step<10) {print STDERR " ";}
    print STDERR"$step/$stepmax      .......\r";
    print STDERR "    Step ";
    if ($step<10) {print STDERR " ";}
    print STDERR"$step/$stepmax      ";
    %proba_forme_sachant_lemme_FL_old=%proba_forme_sachant_lemme_FL;
    %proba_forme_sachant_lemme_FL=();
    %proba_lemme_old=%proba_lemme;
    %proba_lemme=();
    # proba_conj_sachant_cat_CCat
    my $tempvar;
    %proba_conj_sachant_cat_CCat=();
    for $lemme (keys %proba_lemme_old) {
	$proba_conj_sachant_cat_CCat{$conj_de_lemme{$lemme}}{$cat_de_lemme{$lemme}}+=$occ{$lemme};#*$lemmes{$lemme};
	$proba_conj_sachant_cat_CCat{__ALL__}{$cat_de_lemme{$lemme}}+=$occ{$lemme};#*$lemmes{$lemme};
    }
    &normalize_1_2(\%proba_conj_sachant_cat_CCat);
    delete($proba_conj_sachant_cat_CCat{__ALL__});
    # proba_forme_sachant_lemme_FL
    print STDERR ".";
    if ($step>0) {
	for $forme (keys %occurrences) {
	    for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
		if ($proba_lemme_old{$lemme}>0) {
		    $tempvar=0;
		    for $autrelemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
			if ($lemme ne $autrelemme && $autrelemme!~/\#/) {
			    $tempvar+=$proba_lemme_old{$autrelemme}*$proba_forme_sachant_lemme_FL_old{$forme}{$autrelemme};
			}
		    }
		    $proba_forme_sachant_lemme_FL{$forme}{$lemme}=($proba_forme{$forme}-$tempvar)/$proba_lemme_old{$lemme};
		    if ($proba_forme_sachant_lemme_FL{$forme}{$lemme}<0) {$proba_forme_sachant_lemme_FL{$forme}{$lemme}=0}
#		$proba_forme_sachant_lemme_FL{$forme}{$lemme}=(2*$proba_forme_sachant_lemme_FL{$forme}{$lemme}+$proba_term_sachant_conj_TC{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}}{$conj_de_lemme{$lemme}})/3;
		    $proba_forme_sachant_lemme_FL{__ALL__}{$lemme}+=$proba_forme_sachant_lemme_FL{$forme}{$lemme};
		} else {
		    $proba_forme_sachant_lemme_FL{$forme}{$lemme}=0;
		}
	    }
	}
	for $forme (keys %proba_forme_sachant_lemme_FL) { # inutile une fois que l'on a convergé!!!
	    if ($forme ne "__ALL__") {
		for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
		    if ($proba_forme_sachant_lemme_FL{$forme}{$lemme}>0) {
			$proba_forme_sachant_lemme_FL{$forme}{$lemme}/=$proba_forme_sachant_lemme_FL{__ALL__}{$lemme};
		    }
		    &log_print("($step)\tproba_forme_sachant_lemme_FL{$forme}{$lemme}=$proba_forme_sachant_lemme_FL{$forme}{$lemme}\n",2);
		}
	    }
	}
    }
    delete($proba_forme_sachant_lemme_FL{__ALL__});
    # proba_lemme_sachant_forme_LF
    print STDERR ".";
    %proba_lemme_sachant_forme_LF=();
    my $lemmesansvar;
    my $autrelemmesansvar;
    my $multiplicateur=max(2-2*($stepmax-$step)/$stepmax,1);
    for $forme (keys %occurrences) {
	for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
	    $tempvar=0;
	    $lemme=~/^([^:]*)/;
	    $lemmesansvar=$1;
	    for $autrelemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
		$autrelemme=~/^([^:]*)/;
		$autrelemmesansvar=$1;
		if ($autrelemme ne $lemme) {# && $autrelemmesansvar ne $lemmesansvar) {
		    $tempvar+=$proba_lemme_old{$autrelemme}*$proba_forme_sachant_lemme_FL{$forme}{$autrelemme};
		}
	    }
	    if ($tempvar>0) {
#		$proba_lemme_sachant_forme_LF{$lemme}{$forme}=$proba_lemme_old{$lemme}*$proba_forme_sachant_lemme_FL{$forme}{$lemme}/$proba_lemme_sachant_forme_LF{$lemme}{$forme}; ????
		if ($proba_lemme_old{$lemme}==0) {
		    $proba_lemme_sachant_forme_LF{$lemme}{$forme}=0;
		} else {
		    $proba_lemme_sachant_forme_LF{$lemme}{$forme}=$proba_lemme_old{$lemme}*$proba_forme_sachant_lemme_FL{$forme}{$lemme}*(1+$proba_conj_sachant_cat_CCat{$conj_de_lemme{$lemme}}{$cat_de_lemme{$lemme}}/10)/$tempvar;
		    $proba_lemme_sachant_forme_LF{__ALL__}{$forme}+=$proba_lemme_old{$lemme}*$proba_forme_sachant_lemme_FL{$forme}{$lemme}*(1+$proba_conj_sachant_cat_CCat{$conj_de_lemme{$lemme}}{$cat_de_lemme{$lemme}}/10)/$tempvar;
		}
	    } else {
		$proba_lemme_sachant_forme_LF{$lemme}{$forme}=(1+$proba_conj_sachant_cat_CCat{$conj_de_lemme{$lemme}}{$cat_de_lemme{$lemme}}/10);
		$proba_lemme_sachant_forme_LF{__ALL__}{$forme}=(1+$proba_conj_sachant_cat_CCat{$conj_de_lemme{$lemme}}{$cat_de_lemme{$lemme}}/10);
	    }
	    if ($proba_lemme_sachant_forme_LF{__BEST1__}{$forme}<$proba_lemme_sachant_forme_LF{$lemme}{$forme}) {
		$proba_lemme_sachant_forme_LF{__BEST2__}{$forme}=$proba_lemme_sachant_forme_LF{__BEST1__}{$forme};
		$proba_lemme_sachant_forme_LF{__BEST1__}{$forme}=$proba_lemme_sachant_forme_LF{$lemme}{$forme};
	    }
	}
#	for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
#	    if ($proba_lemme_sachant_forme_LF{$lemme}{$forme}==$proba_lemme_sachant_forme_LF{__BEST1__}{$forme}) {
#		$proba_lemme_sachant_forme_LF{__ALL__}{$forme}+=$proba_lemme_sachant_forme_LF{$lemme}{$forme}*($multiplicateur-1);
#		$proba_lemme_sachant_forme_LF{$lemme}{$forme}*=$multiplicateur;
#		$proba_lemme_sachant_forme_LF{__BEST1__}{$forme}*=$multiplicateur;
#	    }
#	}

## POURQUOI FALLAIT-IL FAIRE CA ???????
##	$proba_lemme_sachant_forme_LF{__BEST1__}{$forme}/=$proba_lemme_sachant_forme_LF{__ALL__}{$forme};
##	$proba_lemme_sachant_forme_LF{__BEST2__}{$forme}/=$proba_lemme_sachant_forme_LF{__ALL__}{$forme};
    }
    &normalize_1_2(\%proba_lemme_sachant_forme_LF);
    delete($proba_lemme_sachant_forme_LF{__ALL__});
    # occurrences des lemmes
    %occ=();
    for $forme (keys %occurrences) {
#	log_print("### $forme\n",3);
	for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
	    &log_print("($step)\tproba_lemme_sachant_forme_LF{$lemme}{$forme}=$proba_lemme_sachant_forme_LF{$lemme}{$forme}\n",1);
	    if ($odds_lemme{$lemme}>0) {
		$occ{$lemme}+=$occurrences{$forme}*$proba_lemme_sachant_forme_LF{$lemme}{$forme};
#		log_print("    \$occ{$lemme}+=$occurrences{$forme}*$proba_lemme_sachant_forme_LF{$lemme}{$forme}      [$forme]\n",3);
	    }
	}
    }
    # proba_forme_et_lemme_FL
    print STDERR ".";
    %proba_forme_et_lemme_FL=();
    my $pfltotal=0;
    for $forme (keys %occurrences) {
	for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
	    $proba_forme_et_lemme_FL{$forme}{$lemme}=$proba_forme{$forme}*$proba_lemme_sachant_forme_LF{$lemme}{$forme};
	    if ($lemme!~/\#/) {
		$pfltotal+=$proba_forme_et_lemme_FL{$forme}{$lemme};
	    }
	}
    }
    %proba_forme=();
    for $forme (keys %occurrences) {
	for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
	    &log_print("($step)\t\$proba_forme_et_lemme_FL{$forme}{$lemme}=$proba_forme_et_lemme_FL{$forme}{$lemme}/$pfltotal\t= ",1);
	    $proba_forme_et_lemme_FL{$forme}{$lemme}/=$pfltotal;
	    if ($lemme!~/\#/) {
		$proba_forme{$forme}+=$proba_forme_et_lemme_FL{$forme}{$lemme};
	    }
	    &log_print("$proba_forme_et_lemme_FL{$forme}{$lemme}\n",1);
	}
    }
#    if ($step<$stepmax) {
#	%proba_lemme_sachant_forme_LF=();
#    }
    print STDERR ".";
    # proba_tag_sachant_cat_TC + proba_term_sachant_conj_et_tag_CTagT
    %proba_tag_sachant_cat_TC=();
    %proba_term_sachant_conj_et_tag_CTagT=();
    for $forme (keys %proba_forme_et_lemme_FL) {
	for $lemme (keys %{$proba_forme_et_lemme_FL{$forme}}) {
	    $cat=$cat_de_lemme{$lemme};
	    $conj=$conj_de_lemme{$lemme};
	    for $tag (keys %{$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}}) {
		my $t=$proba_forme_et_lemme_FL{$forme}{$lemme}*$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag};
#		print STDERR "forme=$forme\tlemme=$lemme\ttag=$tag\tcat=$cat\tconj=$conj\tt=$t (=$proba_forme_et_lemme_FL{$forme}{$lemme}*$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag})\n";
		$proba_tag_sachant_cat_TC{$tag}{$cat}+=$t;
		$proba_tag_sachant_cat_TC{__ALL__}{$cat}+=$t;
		$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}}+=$t;
		$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{__ALL__}+=$t;
	    }
	}
    }
    for $lemme (keys %odds_lemme) {
	$cat=$cat_de_lemme{$lemme};
	$conj=$conj_de_lemme{$lemme};
	for $forme (keys %{$formes_absentes_issues_de_lemme_LF{$lemme}}) {
	    for $tag (keys %{$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}}) {
		if ($proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{__ALL__}==0) {
		    $proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}}=-1;
		}
	    }
	}
    }
    for $conj (keys %proba_term_sachant_conj_et_tag_CTagT) {
	for $tag (keys %{$proba_term_sachant_conj_et_tag_CTagT{$conj}}) {
	    for $term (keys %{$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}}) {
		if ($proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term}==-1) {
		    $proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term}=1;
		    $proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{__ALL__}+=1;
		}
	    }
	}
    }
    &normalize_1_2(\%proba_tag_sachant_cat_TC);
    delete($proba_tag_sachant_cat_TC{__ALL__});
#    for $tag (keys %proba_tag_sachant_cat_TC) {
#	log_print("TTTT $tag :\n",3);
#	for $cat (keys %{$proba_tag_sachant_cat_TC{$tag}}) {
#	    &log_print("TTTT \$proba_tag_sachant_cat_TC{$tag}{$cat}=$proba_tag_sachant_cat_TC{$tag}{$cat}\n",3);
#	}
#    }
    &normalize_3_3(\%proba_term_sachant_conj_et_tag_CTagT);
    # proba_term_sachant_conj_TC
    %proba_term_sachant_conj_TC=();
    for $conj (keys %proba_conj_sachant_cat_CCat) {
	$conj=~/^([^-]+)/;
	$cat=$1;
	for $tag (keys %{$proba_term_sachant_conj_et_tag_CTagT{$conj}}) {
	    for $term (keys %{$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}}) {
		my $t=$proba_tag_sachant_cat_TC{$tag}{$cat}*$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term};
#		print STDERR "\$proba_term_sachant_conj_TC{$term}{$conj}+=$proba_tag_sachant_cat_TC{$tag}{$cat}*$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term} ($conj / $tag / $term)\n";
		$proba_term_sachant_conj_TC{$term}{$conj}+=$t;
		$proba_term_sachant_conj_TC{__ALL__}{$conj}+=$t;
	    }
	}
    }
    &normalize_1_2(\%proba_term_sachant_conj_TC);
    delete($proba_term_sachant_conj_TC{__ALL__});
    %proba_conj_sachant_cat_CCat=();
#    for $conj (keys %proba_conj_sachant_cat_CCat) {
#	log_print("CCCC $conj :\n",3);
#	for $term (keys %proba_term_sachant_conj_TC) {
#	    if (defined $proba_term_sachant_conj_TC{$term}{$conj}) {
#		log_print("CCCC \$proba_term_sachant_conj_TC{$term}{$conj}=$proba_term_sachant_conj_TC{$term}{$conj}\n",3);
#	    }
#	}
#    }
    # upgrade des odds
    print STDERR ".";
    %proba_lemme=();
    my $contrib;
    my $contribAutresConj;
    for $lemme (keys %odds_lemme) {
	if (defined($lemmes_connus{$lemme})) {
	    $odds_lemme{$lemme}=10000000000000;
	    $lemme_certain{$lemme}=1;
	    &log_print("($step)\t$lemme\tDEJA CONNU\n",1);
	} else {
	    $odds_lemme{$lemme}=1;
	}
    }
    my $lemmebrut; #=$lemme si pas à préfixe, =le lemme sans "#" si à préfixe
    if ($corpus_tagge) {$moderateur=1} else {$moderateur=min(1,$step/8)}
    for $lemme (keys %odds_lemme) {
	if (!defined($lemme_certain{$lemme})) {
	    $lemmebrut=$lemme;
	    $lemmebrut=~s/\#//;
	    $contrib=$proba_forme{$forme};
	    for $forme (keys %{$formes_issues_de_lemme_LF{$lemme}}) {
#		$contrib=0;
#		for $autrelemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
#		    if ($autrelemme ne $lemmebrut
#			&& $autrelemme!~/\#/) { # PAS SI SUR
#			$contrib+=$proba_forme_et_lemme_FL{$forme}{$autrelemme};
#		    }
#		}
		$contrib=$proba_forme{$forme};
		$contribAutresConj=$contrib-$proba_forme_et_lemme_FL{$forme}{$lemme};
		if ($lemmebrut ne $lemme) {
		    $contribAutresConj-=$proba_forme_et_lemme_FL{$forme}{$lemmebrut};
		}
		if ($contribAutresConj>0.0000001 && !defined($lemme_certain{$lemme})) {
		    $odds_lemme{$lemme}*=1+$moderateur*($contrib/$contribAutresConj-1);
#		    $odds_lemme{$lemme}*=($contrib+$proba_forme_et_lemme_FL{$forme}{$lemme})/$contrib;
		    &log_print("($step)\t$lemme\t$forme\todds_lemme{$lemme}*=1+$moderateur*($contrib/$contribAutresConj-1)=".1+$moderateur*($contrib/$contribAutresConj-1)." => $odds_lemme{$lemme}\n",1);
		} elsif ($proba_forme_et_lemme_FL{$forme}{$lemme}>$contribAutresConj) {
		    &log_print("($step)\t$lemme\t$forme\tCERTAIN\n",1);
		    $odds_lemme{$lemme}=10000000000000;
		    $lemme_certain{$lemme}=1;
		    print TRACE "$lemme\t$forme\n";
		} else {
		    &log_print("($step)\t$lemme\t$forme\todds_lemme{$lemme} inchangé (\$proba_forme_et_lemme_FL{$forme}{$lemme}=$proba_forme_et_lemme_FL{$forme}{$lemme}) => $odds_lemme{$lemme}\n",1);
		}
	    }
	} else {
	    &log_print("($step)\t$lemme\tDEJA CERTAIN\n",1);
	    $odds_lemme{$lemme}=10000000000000;
	}
    }
    %proba_forme_et_lemme_FL=();
    for $lemme (keys %odds_lemme) {
	if (!defined($lemme_certain{$lemme})) {
	    for $forme (keys %{$formes_absentes_issues_de_lemme_LF{$lemme}}) {
		$odds_lemme{$lemme}*=(1-$proba_term_sachant_conj_TC{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}}{$conj_de_lemme{$lemme}}) ** (max(1,$occ{$lemme})*$moderateur);
#?
		&log_print("($step)\t$lemme\t$forme (-)\todds_lemme{$lemme}*=(1-\$proba_term_sachant_conj_TC{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}}{$conj_de_lemme{$lemme}}=$proba_term_sachant_conj_TC{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}}{$conj_de_lemme{$lemme}})**".max(1,$occ{$lemme})."*".$moderateur." => $odds_lemme{$lemme}\n",1);
	    }
	} else {
	    &log_print("($step)\t$lemme\tDEJA CERTAIN\n",1);
	    $odds_lemme{$lemme}=10000000000000;
	}
    }
    # morphologie derivtionnelle nouvelle mode
    my $lemmesansdiese;
    my $lemmedersansdiese;
    my %proba_derive_valide_LLd_old=%proba_derive_valide_LLd;;
    my $tempvar2;
    %proba_derive_valide_LLd=();
    for $lemme (keys %lemme_derive_de_lemme_LLd) {
#	print STDERR "<$lemme>\n";
	for my $lemmeder (keys %{$lemme_derive_de_lemme_LLd{$lemme}}) {
#	    print STDERR "    <$lemmeder>\n";
	    if (defined($odds_lemme{$lemme}) && defined($odds_lemme{$lemmeder})) {
		$tempvar=$odds_lemme{$lemme};
		$tempvar2=$proba_derive_valide_LLd_old{$lemme}{$lemmeder}/($lemme_derive_de_lemme_LLd{$lemme}{$lemmeder}+1);
		$odds_lemme{$lemme}*=1+$tempvar2*(min($odds_lemme{$lemmeder},2)+1);
		if ($odds_lemme{$lemme}>10000000) {$odds_lemme{$lemme}=10000000000000; $lemme_certain{$lemme}=1;}
		$odds_lemme{$lemmeder}*=1+$tempvar2*($tempvar+1);
		if ($odds_lemme{$lemmeder}>10000000) {$odds_lemme{$lemmeder}=10000000000000; $lemme_certain{$lemmeder}=1;}
		$tempvar=sqrt($odds_lemme{$lemme}*$odds_lemme{$lemmeder});
		$proba_derive_valide_LLd{$lemme}{$lemmeder}=$tempvar/(1+$tempvar);
		$proba_derive_valide_LLd{__ALL__}{$lemmeder}+=$tempvar/(1+$tempvar);
		&log_print("\n\$proba_derive_valide_LLd{$lemme}{$lemmeder}=$proba_derive_valide_LLd{$lemme}{$lemmeder}",1);
		if ($lemme=~/#/) {
		    $lemmesansdiese=$lemme;
		    $lemmesansdiese=~s/#//;
		    $proba_derive_valide_LLd{$lemmesansdiese}{$lemmeder}=$proba_derive_valide_LLd{$lemme}{$lemmeder};
		    $proba_derive_valide_LLd{__ALL__}{$lemmeder}+=$proba_derive_valide_LLd{$lemme}{$lemmeder};
		    &log_print("\n\$proba_derive_valide_LLd{$lemmesansdiese}{$lemmeder}=$proba_derive_valide_LLd{$lemmesansdiese}{$lemmeder}",1);
		    if ($lemmeder=~/#/) {
			$lemmedersansdiese=$lemmeder;
			$lemmedersansdiese=~s/#//;
			$proba_derive_valide_LLd{$lemmesansdiese}{$lemmedersansdiese}=$proba_derive_valide_LLd{$lemme}{$lemmeder};
			&log_print("\n\$proba_derive_valide_LLd{$lemmesansdiese}{$lemmedersansdiese}=$proba_derive_valide_LLd{$lemmesansdiese}{$lemmedersansdiese}",1);
			$proba_derive_valide_LLd{__ALL__}{$lemmedersansdiese}+=$proba_derive_valide_LLd{$lemme}{$lemmeder};
		    }
		}
	    }
	}
    }
    &normalize_1_2(\%proba_derive_valide_LLd);
    delete($proba_derive_valide_LLd{__ALL__});
    # on aime les contreparties sans préfixes de lemmes à préfixes
    for $lemme (keys %lemmes) {
	if ($lemme =~ /^(.*)\#(.*)$/) {
	    $pref=$1;
	    $rad=$2;
	    $odds_lemme{$rad}*=$probapref{$pref}*$odds_lemme{$pref.$rad}+(1-$probapref{$pref});
	}
    }
    print STDERR ".";
    # màj des probas de lemmes à partir des odds
    my $eliminated_lemmas=0;
    my $saviourform;
    for $lemme (keys %odds_lemme) {
	if ($odds_lemme{$lemme}>=1000000000000) {
	    $lemmes{$lemme}=1;
	} else {
	    $lemmes{$lemme}=($odds_lemme{$lemme}/(1+$odds_lemme{$lemme}));
	}
	$proba_lemme{$lemme}=$occ{$lemme}*$lemmes{$lemme}/$nmots;
	if ($proba_lemme{$lemme}<=$proba_lemme_eliminatoire) {
	    $saviourform="";
	    for $forme (keys %{$proba_lemme_sachant_forme_LF{$lemme}}) {
		if ($proba_lemme_sachant_forme_LF{$lemme}{$forme}>=$proba_lemme_sachant_forme_LF{__BEST2__}{$forme} && $proba_lemme_sachant_forme_LF{$lemme}{$forme}>0.0000001) {
		    $saviourform=$forme;
		}
	    }
	    if ($saviourform eq "" || $step==$stepmax) {
		&log_print("($step)\tproba_lemme{$lemme}<=$proba_lemme_eliminatoire ($occ{$lemme}*$lemmes{$lemme}/$nmots)\tABANDON de ce lemme\n",1);
		$eliminated_lemmas++;
		$eliminated_lemmas_total++;
		delete($proba_lemme{$lemme});
		delete($odds_lemme{$lemme});
		delete($occ{$lemme});
		delete($lemme_certain{$lemme});
		delete($lemmes{$lemme});
		for my $lemmeder (keys %{$lemme_derive_de_lemme_LLd{$lemme}}) {
		    delete($lemme_origine_de_lemme_LdL{$lemmeder}{$lemme});
		}
		for my $lemmeo (keys %{$lemme_origine_de_lemme_LdL{$lemme}}) {
		    delete($lemme_derive_de_lemme_LLd{$lemmeo}{$lemme});
		}
		delete($lemme_derive_de_lemme_LLd{$lemme});
		delete($lemme_origine_de_lemme_LdL{$lemme});
		for $forme (keys %{$formes_issues_de_lemme_LF{$lemme}}) {
		    delete($lemmes_generant_forme_FL{$forme}{$lemme});
		    delete($proba_forme_sachant_lemme_FL{$forme}{$lemme});
		    delete($proba_forme_et_lemme_FL{$forme}{$lemme});
		}
		delete($proba_lemme_sachant_forme_LF{$lemme});
		delete($formes_issues_de_lemme_LF{$lemme});
		delete($formes_absentes_issues_de_lemme_LF{$lemme});
		delete($proba_derive_valide_LLd{$lemme});
	    } else {
		&log_print("($step)\tproba_lemme{$lemme}<=$proba_lemme_eliminatoire ($occ{$lemme}*$lemmes{$lemme}/$nmots)\tREPECHAGE de ce lemme -> proba_lemme{$lemme}=$proba_lemme_eliminatoire (forme salvatrice $saviourform)\n",1);
		$lemmes{$lemme}=min(0.0001,$proba_lemme_eliminatoire*$nmots/10);
		$proba_lemme{$lemme}=$proba_lemme_eliminatoire;
	    }
	} else {
	    &log_print("($step)\tproba_lemme{$lemme}=$proba_lemme{$lemme}\todds_lemme{$lemme}=$odds_lemme{$lemme}\tlemmes{$lemme}=$lemmes{$lemme}\n",1);
	}
    }
    if ($step<$stepmax) {
	%proba_lemme_sachant_forme_LF=();
    }
    # proba_tag_sachant_forme_et_lemme_FLT
    for $forme (keys %occurrences) {
	for $lemme (keys %{$lemmes_generant_forme_FL{$forme}}) {
	    $conj=$conj_de_lemme{$lemme};
	    $cat=$cat_de_lemme{$lemme};
	    my $temp=0; my $t=0;
	    for $tag (keys %{$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}}) {
		$t=$proba_tag_sachant_cat_TC{$tag}{$cat}*$proba_term_sachant_conj_et_tag_CTagT{$conj}{$tag}{$term_de_forme_sachant_lemme_FL{$forme}{$lemme}};
		$temp+=$t;
		$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}=$t;
	    }
	    for $tag (keys %{$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}}) {
		if ($temp==0) {
		    if ($proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}>0) {die "Bug (\$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}=$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag} - $temp=0)\n";}
		} else {
		    $proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}/=$temp;
		}
#		log_print("($step)\t\$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}=$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag}\n",3);
	    }
	}
    }
    print STDERR "\t[$eliminated_lemmas -> $eliminated_lemmas_total/".($nltotal-$nlelimines-$eliminated_lemmas_total)."/".($nltotal-$nlelimines)."]           \n";#\r";
}
print STDERR "    Studying prefixes...                    \r";
my %lemmesprefs=();
my %lemmesder=();
my $tmpint=0;
foreach $lemme (keys %lemmes) {
    if ($lemme=~/^(.*)\#(.*)$/) {
	$pref=$1;
	$rad=$2;
	$l=$pref.$rad;
	if ($lemmes{$l}<=$lemmes{$lemme}) {
	    $lemmes{$l}=1-(1-$lemmes{$l})*(1-$lemmes{$lemme});
	    $occ{$l}=$occ{$lemme}+$occ{$pref.$rad};
	    $lemmesprefs{$l}=$pref;
	    $tmpint=$occ{$l};
#	    $occ{$l}*=1+$occ{$rad}*$probapref{$pref}/5;
#	    $occ{$rad}*=1+$tmpint*$probapref{$pref}/5;
	    &log_print("Analyse de $l comme $lemmesprefs{$l}-$rad   ---   $occ{$l}  ---  $occ{$rad}\n",1);
	}
    }
}
print STDERR "    Studying prefixes...done\n    Studying derivations...";
my $lemmeder;
my $lemmesansdiese;
#for $forme (keys %lemme_derive_de_forme_de_lemme_FL) {
#    for $lemme (keys %{$lemme_derive_de_forme_de_lemme_FL{$forme}}) {
#	for $lemmeder (@{$lemme_derive_de_forme_de_lemme_FL{$forme}{$lemme}}) {
#	    &log_print("Analyse possible de $lemmeder comme lemme dérivé de $lemme...\n",1);
#	    if ($proba_derive_valide_LLd{$lemme}{$lemmeder}<$proba_lemme_sachant_forme_LF{$lemme}{$forme}) {
#		$proba_derive_valide_LLd{$lemme}{$lemmeder}=$proba_lemme_sachant_forme_LF{$lemme}{$forme};
#		if ($lemme=~/#/) {
#		    $lemmesansdiese=$lemme;
#		    $lemmesansdiese=~s/#//;
#		    $proba_derive_valide_LLd{$lemmesansdiese}{$lemmeder}=$proba_derive_valide_LLd{$lemme}{$lemmeder};
#		}
#	    }
#	}
#    }
#}
# �tude des d�riv�s
$donesomething=1;
while ($donesomething) {
    $donesomething=0;
    foreach $lemme (keys %proba_derive_valide_LLd) {
	foreach $lemmeder (keys %{$proba_derive_valide_LLd{$lemme}}) {
#	    print STDERR "\n\$proba_derive_valide_LLd{$lemme}{$lemmeder}=$proba_derive_valide_LLd{$lemme}{$lemmeder}\n";
	    if ($proba_derive_valide_LLd{$lemme}{$lemmeder}>0.3) {
		$lemmesder{$lemme}.=" ".$lemmeder." ".$lemmesder{$lemmeder};
		delete($lemme_derive_de_lemme_LLd{$lemme}{$lemmeder});
		delete($proba_derive_valide_LLd{$lemme}{$lemmeder});
		$occ{$lemme}+=$occ{$lemmeder}*$proba_derive_valide_LLd{$lemme}{$lemmeder};
		$lemmes{$lemmeder}=-1;
		$donesomething=1;
	    }
	}
    }
}
print STDERR "done\n";
foreach $lemme(keys %lemmes) {
    if ($lemmes{$lemme}>0 && 1000*$lemmes{$lemme}<1) {$lemmes{$lemme}=0;}
}
foreach $lemme (keys %lemmes) {
    if ($lemme !~ /\#/) {
	if ($lemmes{$lemme}>0) { # = -1 signifie lemme de'rive'
	    print $lemme."\t".$lemmesprefs{$lemme}."-\t".$lemmes{$lemme}."\t";
	    $lemmes{$lemme}*=(1+log(1+log(1+$occ{$lemme}))/1000);
	    print $occ{$lemme}."\t".$lemmes{$lemme}."\t".$lemmesder{$lemme}."\n";
	}
	for $forme (keys %{$proba_lemme_sachant_forme_LF{$lemme}}) {
	    if ($proba_lemme_sachant_forme_LF{$lemme}{$forme}>0) {
		for $tag (keys %{$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}}) {
		    print TAGDATA "$forme\t$lemme\t$tag\t".($proba_lemme_sachant_forme_LF{$lemme}{$forme}*$proba_tag_sachant_forme_et_lemme_FLT{$forme}{$lemme}{$tag})."\n";
		}
	    }
	}
    }
}
close(TAGDATA);
close(TRACE);
close(GOOGLELOG);
exit(0);

sub normalize_3_3 {
    my $hashref=shift;
    for my $k1 (keys %$hashref) {
	for my $k2 (keys %{$$hashref{$k1}}) {
	    for my $k3 (keys %{$$hashref{$k1}{$k2}}) {
		if ($k3!~/^__/) {
		    if ($$hashref{$k1}{$k2}{__ALL__}==0) {
			if ($$hashref{$k1}{$k2}{$k3}>0) {
			    die "Bug : ".$hashref."{$k1}{$k2}{$k3}>0 alors que {$k1}{$k2}{__ALL__}=0\n";
			}
		    } else {
			if ($$hashref{$k1}{$k2}{$k3}<$$hashref{$k1}{$k2}{__ALL__}/1000000000) {
			    delete($$hashref{$k1}{$k2}{$k3});
			} else {
			    $$hashref{$k1}{$k2}{$k3}/=$$hashref{$k1}{$k2}{__ALL__};
			}
		    }
		}
	    }
	    delete $$hashref{$k1}{$k2}{__ALL__};
	}
    }
#    return %$hashref;
    return 1;
}
sub normalize_1_2 {
    my $hashref=shift;
    for my $k1 (keys %$hashref) {
	if ($k1!~/^__/) {
	    for my $k2 (keys %{$$hashref{$k1}}) {
		if ($$hashref{__ALL__}{$k2}==0) {
		    if ($$hashref{$k1}{$k2}>0) {
			die "Bug : ".$hashref."{$k1}{$k2}>0 alors que {__ALL__}{$k2}=0\n";
		    }
		} else {
		    if ($$hashref{$k1}{$k2}<$$hashref{__ALL__}{$k2}/1000000000) {
			delete($$hashref{$k1}{$k2});
		    } else {
			$$hashref{$k1}{$k2}/=$$hashref{__ALL__}{$k2};
		    }
		}
	    }
	}
    }
#    return %$hashref;
    return 1;
}
sub log_print {
    my $string=shift;
    my $level=shift;
    if ($level<=$loglevel) {
	print LOG $string;
    }
}
