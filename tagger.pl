#!/usr/bin/env perl

use strict;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use open ':utf8';

my $tagbigramsfile=shift;
my $stoplistfile=shift;
my $validatedformsfile=shift;
my $add_uncertainty=shift;

print STDERR "TAGGER --- tagbigramsfile=$tagbigramsfile stoplistfile=$stoplistfile\n";

print STDERR "\tLoading bigrams...";
my %proba_rlbigram=();
my %proba_lrbigram=();
my %all_tags_in_bigrams=();
my $deux; my $trois;
open(TAGBIGRAMS,"< $tagbigramsfile") || die("'$tagbigramsfile': $!\n");
while (<TAGBIGRAMS>) {
    chomp;
    /^([<>])\t([^\t]*)\t([^\t]*)\t([^\t]*)$/ || die ("Format error in $tagbigramsfile: $_\n");
    if ($1 eq ">") {
	$proba_lrbigram{$2}{$3}=$4;
	$proba_lrbigram{$2}{__ALL__}+=$4;
    } else {
	$proba_rlbigram{$2}{$3}=$4;
	$proba_rlbigram{$2}{__ALL__}+=$4;
    }
    $deux=$2; $trois=$3;
    if ($deux!~/[:{]/) {$all_tags_in_bigrams{$deux}=1;}
    if ($trois!~/[:{]/) {$all_tags_in_bigrams{$trois}=1;}
}
my $n_tags_in_bigrams=scalar(keys %all_tags_in_bigrams)+1;

my %stoplist_proba_tag_sachant_forme_FT=();
open(STOPLIST,"< $stoplistfile") || die("'$stoplistfile': $!\n");
while (<STOPLIST>) {
    chomp;
    if (/^([^\t]*)\t([^\t]*)$/) {
	$stoplist_proba_tag_sachant_forme_FT{$1}{$2}++;
	$stoplist_proba_tag_sachant_forme_FT{$1}{__ALL__}++;
    }
}
close(STOPLIST);
my $forme; my $tag;
my %validated_form=();
open(VALIDATEDFORMS,"< $validatedformsfile") || die("'$validatedformsfile': $!\n");
while (<VALIDATEDFORMS>) {
    chomp;
    $validated_form{$_}=1;
}
close(VALIDATEDFORMS);
for $forme (keys %stoplist_proba_tag_sachant_forme_FT) {
    for $tag (keys %{$stoplist_proba_tag_sachant_forme_FT{$forme}}) {
	if ($tag ne "__ALL__") {
	    $stoplist_proba_tag_sachant_forme_FT{$forme}{$tag}/=$stoplist_proba_tag_sachant_forme_FT{$forme}{__ALL__}
	}
    }
}
for $forme (keys %stoplist_proba_tag_sachant_forme_FT) {
    delete($stoplist_proba_tag_sachant_forme_FT{$forme}{__ALL__});
}

print STDERR "done\n\tTagging...\r";
my %proba_prevtag_sachant_forme=();
my %proba_tag_sachant_forme=();
my %proba_nexttag_sachant_forme=();
my $prevtok=""; my $tok; my $nexttok="";
my @temp=();
my $prevtag; my $nexttag;
my $tagb; my $proba; my $un_sur_proba;
my $tagslist;
my %tagb_memo;
my %cat_memo;
my $proba_gauche; my $proba_droite;
my $proba_tag_sachant_forme_norm;
my $l=0;
my %rlbigram_memo;
my %lrbigram_memo;
my $redistributed_proba;
while (<STDIN>) {
    if ($l % 1000 == 0) {print STDERR "\tTagging...$l\r"}
    $l++;
    chomp;
    if (/./) {
	# lecture de l'entr�e
	$prevtok=$tok;
	$tok=$nexttok;
	%proba_prevtag_sachant_forme=%proba_tag_sachant_forme;
	%proba_tag_sachant_forme=%proba_nexttag_sachant_forme;
	/^([^\t]*)\t(.*)$/;
	$nexttok=$1;
	%proba_nexttag_sachant_forme=();
	@temp=split(/\|/,$2);
	$redistributed_proba=0;
	for my $i (0..$#temp) {
	    $temp[$i]=~/^([^\t]*)\t([^\t]*)$/ || die ("Bad format in input file: ltp $_\n");
	    $tag=$1;
	    $proba=$2;
	    if ($tag eq "?" || ($tag!~/^{/ && !defined($validated_form{$tok}) && $add_uncertainty)) {
		$proba_nexttag_sachant_forme{$tag}=$proba/2;
		$redistributed_proba+=$proba/2;
	    } else {
		$proba_nexttag_sachant_forme{$tag}=$proba;
	    }
	}
	if ($redistributed_proba>0) {
	    for (keys %all_tags_in_bigrams) {
		$proba_nexttag_sachant_forme{$_}+=$redistributed_proba*(1/$n_tags_in_bigrams);
	    }
	}
# 	print STDERR "$tok\n";
# 	if ($tok=~/komunik/) {
# 	    for $tag (keys %proba_tag_sachant_forme) {
# 		print STDERR "proba ici {$tag} = ".$proba_nexttag_sachant_forme{$tag}."\n";
# 		for $prevtag (keys %proba_prevtag_sachant_forme) {
# 		    print STDERR "\t\tbigramme< {prev=$prevtag}{cur=$tag} : ".&contrib_lrbigram($prevtag,$tag)."\n";
# 		}
# 		for $nexttag (keys %proba_nexttag_sachant_forme) {
# 		    print STDERR "\t\tbigramme> {next=$nexttag}{cur=$tag} : ".&contrib_rlbigram($nexttag,$tag)."\n";
# 		}
# 	    }
# 	}

	# modif des probas d'entr�e pour produire les probas de sortie
	if ($prevtok ne "") {
	    $proba_tag_sachant_forme_norm=0;
	    for $tag (keys %proba_tag_sachant_forme) {
		#si on n'est pas d�j� s�rs de nous:
		if ($proba_tag_sachant_forme{$tag}<1) {
		    if ($proba_tag_sachant_forme{$tag}==0) {
			delete($proba_tag_sachant_forme{$tag});
		    } else {
			# on calcule dans $proba_droite et proba_gauche la probabilit� du tag courant sachant le mot pr�c�dent/suivant (et ses probas)
			$proba_gauche=0;
			$proba_droite=0;
#		    print STDERR "\$proba_lemme_et_tag_sachant_forme_LT{$lemme}{$tag}=$proba_lemme_et_tag_sachant_forme_LT{$lemme}{$tag}\n";
			for $prevtag (keys %proba_prevtag_sachant_forme) {
			    $proba_gauche+=&contrib_lrbigram($prevtag,$tag)*$proba_prevtag_sachant_forme{$prevtag};
#			print "\t\tbigramme< {prev=$prevtag}{cur=$tag} : ".&contrib_lrbigram($prevtag,$tag)."*$proba_prevtag_sachant_forme{$prevtag}\n";
			}
			for $nexttag (keys %proba_nexttag_sachant_forme) {
			    $proba_droite+=&contrib_rlbigram($nexttag,$tag)*$proba_nexttag_sachant_forme{$nexttag};
#			print "\t\tbigramme> {next=$nexttag}{cur=$tag} : ".&contrib_rlbigram($nexttag,$tag)."*$proba_nexttag_sachant_forme{$nexttag}\n";
			}
			if ($proba_droite>0 && $proba_gauche>0) {
			    $proba=1/(1/$proba_gauche+1/$proba_droite)/2;
#		    print STDERR "\t$tok\t$tag\t+=($proba_gauche*$proba_droite)( -> $proba) * $proba_tag_sachant_forme{$tag}\n";
			    $proba_tag_sachant_forme{$tag}=($proba+$proba_tag_sachant_forme{$tag})/2;
			    $proba_tag_sachant_forme_norm+=$proba_tag_sachant_forme{$tag};
			} else {
			    delete($proba_tag_sachant_forme{$tag});
			}
#		    $proba_lemme_et_tag_sachant_forme_LT{$lemme}{$tag}*=($proba_gauche+$proba_droite)/2;
		    }
		}
	    }
	    if ($proba_tag_sachant_forme_norm == 0) { #aucun bigramme ne convenait, on rend les tags �quiprobables
		for $tag (keys %proba_tag_sachant_forme) {
		    $proba_tag_sachant_forme{$tag}++;
		    $proba_tag_sachant_forme_norm++;
		}
						    }
	    # normalisation et filtrage des bigrammes de poids trop faible
	    if ($proba_tag_sachant_forme_norm>0) {
		for $tag (keys %proba_tag_sachant_forme) {
		    $proba_tag_sachant_forme{$tag}/=$proba_tag_sachant_forme_norm; # normalisation et filtrage
		    if ($proba_tag_sachant_forme{$tag}<0.00003) {delete($proba_tag_sachant_forme{$tag})}
		}
	    }
	    # nouvelle normalisation (rendue n�cessaire par le filtrage pr�c�dent)
	    $proba_tag_sachant_forme_norm=0;
	    for $tag (keys %proba_tag_sachant_forme) {
		$proba_tag_sachant_forme_norm+=$proba_tag_sachant_forme{$tag};
	    }
	    if ($proba_tag_sachant_forme_norm>0) {
		for $tag (keys %proba_tag_sachant_forme) {
		    $proba_tag_sachant_forme{$tag}/=$proba_tag_sachant_forme_norm; 
		}
	    }
	} else {} # premier mot du fichier

	# output
	if ($tok ne "") {
	    $tagslist="";
	    for $tag (keys %proba_tag_sachant_forme) {
		$proba=$proba_tag_sachant_forme{$tag};
		$tagslist.="|".$tag."\t".$proba;
	    }
	    $tagslist=~s/^\|//;
	    print $tok."\t".$tagslist."\n";
	}

    }
}
print STDERR "\tTagging...done            \n";

sub contrib_lrbigram {
    my $pt=shift;
    my $t=shift;
    if (defined($lrbigram_memo{$pt}) && defined($lrbigram_memo{$pt}{$t})) {
	return $lrbigram_memo{$pt}{$t};
    }
    my $answer=0;
    my $subtagn=-1;
    my $mult;
    if ($t=~/-/) {
	for (split(/-/,$t)) {
	    $subtagn++;
	    if (/./) {
		$mult=$proba_lrbigram{$pt}{$subtagn.":".$_};
		while (s/^.//) {
		    $mult+=$proba_lrbigram{$pt}{$subtagn.":".$_};
		}
		if ($mult>0) {
		    $answer+=1/$mult;
		} else {
		    $answer=0;
		    last;
		}
	    }
	}
	if ($answer>0) {
	    $subtagn++;
	    $answer=($subtagn/$answer);
	}
	$answer+=$proba_lrbigram{$pt}{$t};
	$answer/=2;
    } else {
	$answer=$proba_lrbigram{$pt}{$t};	
    }
    $lrbigram_memo{$pt}{$t}=$answer;
    return $answer;
}

sub contrib_rlbigram {
    my $nt=shift;
    my $t=shift;
    my $answer=1;
    my $subtagn=0;
    if (defined($rlbigram_memo{$nt}) && defined($rlbigram_memo{$nt}{$t})) {
	return $rlbigram_memo{$nt}{$t};
    }
    my $mult;
    if ($t=~/-/) {
	for (split(/-/,$t)) {
	    if (/./) {
		$mult=$proba_rlbigram{$nt}{$subtagn.":".$_};
		while (s/^.//) {
		    $mult+=$proba_rlbigram{$nt}{$subtagn.":".$_};
		}
		if ($mult>0) {
		    $answer+=1/$mult;
		} else {
		    $answer=0;
		    last;
		}
	    }
	}
	if ($answer>0) {
	    $subtagn++;
	    $answer=($subtagn/$answer);
	}
	$answer+=$proba_rlbigram{$nt}{$t};
	$answer/=2;
    } else {
	$answer=$proba_rlbigram{$nt}{$t};	
    }
    $rlbigram_memo{$nt}{$t}=$answer;
    return $answer;
}
